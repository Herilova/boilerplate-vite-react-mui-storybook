import { GridPaginationModel, GridSortModel } from '@mui/x-data-grid';
import { useCallback, useMemo, useState } from 'react';

const PAGE_SIZE_OPTIONS = [20, 30];
export default () => {
  const [sort, setSort] = useState<GridSortModel>([]);
  const [pagination, setPagination] = useState<GridPaginationModel>({
    page: 0,
    pageSize: PAGE_SIZE_OPTIONS[0],
  });
  const [maxRow, setMaxRow] = useState(pagination.pageSize);
  const calculateMaxRowCount = useCallback(
    (newSize?: any[]) => {
      setMaxRow(prev =>
        newSize?.length && newSize.length === pagination.pageSize
          ? (pagination.page + 2) * pagination.pageSize
          : prev
      );
    },
    [pagination.page, pagination.pageSize]
  );

  const sortParams = useMemo(
    () =>
      sort.length
        ? JSON.stringify(
            sort.map(i => ({
              id: i.field,
              desc: i.sort === 'desc',
            }))
          )
        : undefined,
    [sort]
  );
  const paginationParams = useMemo(
    () => ({
      'page-size': pagination.pageSize.toString(),
      'page-number': (pagination.page + 1).toString(),
    }),
    [pagination]
  );
  return {
    dataGridProps: {
      rowCount: maxRow,
      sortModel: sort,
      onSortModelChange: setSort,
      paginationModel: pagination,
      onPaginationModelChange: setPagination,
      pageSizeOptions: PAGE_SIZE_OPTIONS,
    },
    sortParams,
    paginationParams,
    calculateMaxRowCount,
  };
};
