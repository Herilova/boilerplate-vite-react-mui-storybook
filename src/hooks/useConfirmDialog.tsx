import {
  Button,
  ButtonProps,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  Stack,
} from '@mui/material';
import {
  cloneElement,
  createContext,
  MouseEvent,
  ReactElement,
  ReactNode,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import { useTranslation } from 'react-i18next';

interface IConfirmOptions {
  cancelButtonMessage?: string;
  cancelButton?: ReactElement<ButtonProps>;
  confirmButtonMessage?: string;
  confirmButton?: ReactElement<ButtonProps>;
}
interface IActionOptions {
  actions?: (string | ReactElement<ButtonProps>)[];
  actionsD?: (string | ReactElement<ButtonProps>)[][];
}
interface IStateConfirm extends IConfirmOptions, IActionOptions {
  prompt: string | ReactNode;
  isOpen: boolean;
  type?: 'confirm' | 'action';
  confirm?: (value: any | PromiseLike<any>) => void;
  cancel?: () => void;
}
const initialValue: IStateConfirm = {
  prompt: '',
  isOpen: false,
  type: undefined,
};
interface IDialogConfirmContext {
  confirm?: IStateConfirm;
  setConfirm?: (v: IStateConfirm) => void;
}
export const DialogConfirmContext = createContext<IDialogConfirmContext>({});

function ElementButton(props: {
  value: string | ReactElement<ButtonProps>;
  index: number;
  isPrimary: boolean;
  isFullWidth?: boolean;
  handleConfirm: (value?: any | PromiseLike<any>) => () => void;
}) {
  const { value, index, isPrimary, isFullWidth = false, handleConfirm } = props;
  if (typeof value === 'string') {
    return (
      <Button
        id={`bConfirmDialog${value}`}
        key={`${index}${value}`}
        onClick={handleConfirm(index)}
        fullWidth={isFullWidth}
        variant={isPrimary ? 'contained' : 'outlined'}
        color={isPrimary ? 'primary' : 'secondary'}
      >
        {value || index}
      </Button>
    );
  }
  return (
    <Button
      {...value.props}
      id={`bConfirmDialog${value}`}
      key={`${index}${value}`}
      fullWidth={isFullWidth}
      onClick={e => {
        if (value?.props?.onClick) value.props.onClick(e);
        handleConfirm(index)();
      }}
      variant={isPrimary ? 'contained' : 'outlined'}
      color={isPrimary ? 'primary' : 'secondary'}
    />
  );
}

export const DialogConfirmContextProvider = ({
  children,
}: {
  children: ReactNode;
}): ReactElement => {
  const { t } = useTranslation();
  const [state, setState] = useState<IStateConfirm>(initialValue);
  const handleClose = () => {
    if (state?.cancel) state.cancel();
  };
  const handleConfirm = (value?: any | PromiseLike<any>) => () => {
    if (state?.confirm) state.confirm(value);
  };

  return (
    <DialogConfirmContext.Provider
      value={{
        confirm: state,
        setConfirm: setState,
      }}
    >
      {children}
      {state.isOpen && state.prompt && (
        <Dialog maxWidth="xl" open={state.isOpen} onClose={handleClose}>
          <DialogContent>
            {typeof state.prompt === 'string' &&
            state.prompt.trim().length &&
            state.prompt.charAt(state.prompt.length - 1) !== '?' ? (
              <DialogContentText>{`${state.prompt} ?`}</DialogContentText>
            ) : (
              state.prompt
            )}
          </DialogContent>
          <DialogActions
            style={{
              paddingLeft: 20,
              paddingRight: 20,
              paddingBottom: 10,
            }}
          >
            {state.type === 'action' && (
              <Stack
                style={{
                  width: '100%',
                }}
                flexDirection="column"
              >
                <Stack
                  flexDirection="row"
                  style={{
                    width: '100%',
                  }}
                  justifyContent="flex-end"
                  columnGap="4px"
                >
                  {(state.actions || []).map((value, index) => {
                    const isPrimary = Boolean(
                      (state.actions && state.actions?.length === 1) ||
                        (index >= 1 && state.actions && state.actions.length > 1)
                    );
                    return ElementButton({
                      value,
                      index,
                      isPrimary,
                      isFullWidth: true,
                      handleConfirm,
                    });
                  })}
                </Stack>
                {(state.actionsD || []).map((tab, i) => (
                  <Stack key={`${i.toString()}`} flexDirection="row" marginTop={1} columnGap="4px">
                    {tab.map((value, j) => {
                      if (state.actionsD) {
                        const prevCount =
                          i === 0
                            ? 0
                            : state.actionsD.reduce(
                                (acc, item, ind) => acc + (ind < i ? item.length : 0),
                                0
                              );
                        const index = prevCount + j;
                        const isPrimary = Boolean(
                          (tab && tab.length === 1) || (index >= 1 && tab && tab.length > 1)
                        );
                        return ElementButton({
                          value,
                          index,
                          isPrimary,
                          isFullWidth: true,
                          handleConfirm,
                        });
                      }
                      return undefined;
                    })}
                  </Stack>
                ))}
              </Stack>
            )}

            {state.type === 'confirm' && (
              <Stack
                style={{
                  width: '100%',
                }}
                flexDirection="row"
                columnGap="4px"
              >
                {state.cancelButton ? (
                  cloneElement(state.cancelButton, {
                    onClick: (e: MouseEvent<HTMLButtonElement>) => {
                      if (state?.cancelButton?.props?.onClick) state.cancelButton.props.onClick(e);
                      handleConfirm(false)();
                    },
                    color: 'primary',
                    variant: 'outlined',
                    fullWidth: true,
                  })
                ) : (
                  <Button
                    id={`bCancelDialog${state.cancelButtonMessage}`}
                    onClick={handleConfirm(false)}
                    variant="outlined"
                    color="primary"
                    fullWidth={true}
                  >
                    {state.cancelButtonMessage || t('no')}
                  </Button>
                )}

                {state.confirmButton ? (
                  cloneElement(state.confirmButton, {
                    onClick: (e: MouseEvent<HTMLButtonElement>) => {
                      if (state?.confirmButton?.props?.onClick)
                        state.confirmButton.props.onClick(e);
                      handleConfirm(true)();
                    },
                    color: 'primary',
                    variant: 'contained',
                    fullWidth: true,
                  })
                ) : (
                  <Button
                    id={`bConfirmDialog${state.confirmButton}`}
                    variant="contained"
                    onClick={handleConfirm(true)}
                    color="primary"
                    fullWidth={true}
                  >
                    {state.confirmButtonMessage || t('yes')}
                  </Button>
                )}
              </Stack>
            )}
          </DialogActions>
        </Dialog>
      )}
    </DialogConfirmContext.Provider>
  );
};

interface IUseDialog {
  confirm: (m: string | ReactNode, options?: IConfirmOptions) => Promise<boolean | undefined>;
  action: (m: string | ReactNode, options: IActionOptions) => Promise<number | undefined>;
}
/**
 * Custom hooks dialog prompt | action | confirm
 */
export function useConfirmDialog(): IUseDialog {
  const { setConfirm, confirm } = useContext(DialogConfirmContext);
  const needsCleanupRef = useRef(false);

  useEffect(() => {
    return () => {
      if (confirm?.cancel && needsCleanupRef.current) {
        confirm?.cancel();
        needsCleanupRef.current = false;
      }
    };
  }, [confirm]);

  function promiseFn<T>(v: IStateConfirm): Promise<T | undefined> {
    needsCleanupRef.current = true;
    const promise = new Promise<T>((resolve, reject) => {
      if (setConfirm)
        setConfirm({
          ...v,
          confirm: resolve,
          cancel: reject,
        });
    });
    return promise.then(
      (s: T) => {
        if (setConfirm && confirm)
          setConfirm({
            ...confirm,
            ...initialValue,
          });
        return s;
      },
      () => {
        if (setConfirm && confirm) setConfirm({ ...confirm, ...initialValue });
        return undefined;
      }
    );
  }
  const confirmFn: IUseDialog['confirm'] = (prompt, options) => {
    return promiseFn<boolean>({
      prompt,
      ...options,
      isOpen: true,
      type: 'confirm',
    });
  };

  const actionFn: IUseDialog['action'] = (prompt, options) => {
    return promiseFn<number>({
      prompt,
      ...options,
      isOpen: true,
      type: 'action',
    });
  };
  return {
    confirm: confirmFn,
    action: actionFn,
  };
}

export default useConfirmDialog;
