import { useMediaQuery, useTheme } from '@mui/material';

export default () => {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints.down('sm'));
};
