import { useAccount, useMsal } from '@azure/msal-react';

export default () => {
  const { accounts } = useMsal();
  const account = useAccount(accounts[0] || {});
  return account;
};
