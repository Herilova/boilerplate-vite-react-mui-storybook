import { enUS, frFR } from '@mui/material/locale';
import { createTheme as createThemeMui } from '@mui/material/styles';
import { enUS as enUSDG, frFR as frFRDG } from '@mui/x-data-grid';
import { enUS as enUSDP, frFR as frFRDP } from '@mui/x-date-pickers/locales';

export const createTheme = (languageCode: 'fr' | 'en' | string) =>
  createThemeMui(
    {
      palette: {
        primary: {
          main: '#18E8B5',
          light: '#18E8B5',
          dark: '#004F3B',
          // contrastText: '#264e3c',
        },
        secondary: {
          main: '#B583FE',
          light: '#B583FE',
          dark: '#3B2659',
          // contrastText: '#382856',
        },
        error: {
          main: '#FF3A00',
          light: '#FF3A00',
          dark: '#591O0A',
          contrastText: '#FFFFFF',
        },
      },
      typography: {
        fontFamily: ['Roboto', 'sans-serif'].join(','),
      },
      components: {
        MuiButton: {
          styleOverrides: {
            root: {
              textTransform: 'initial',
            },
            outlined: {
              borderWidth: '2px!important',
            },
          },
        },
      },
    },
    languageCode === 'fr' ? frFRDG : enUSDG,
    languageCode === 'fr' ? frFRDP : enUSDP,
    languageCode === 'fr' ? frFR : enUS
  );

export default createTheme;
