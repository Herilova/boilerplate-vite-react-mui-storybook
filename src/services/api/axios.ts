import axios from 'axios';
import config from '../../config';
import { getToken } from '../../context/token';

const axiosInstance = axios.create({
  baseURL: config.baseUrl,
  timeout: 1000,
});

axiosInstance.interceptors.request.use(request => {
  request.headers.Authorization = `Bearer ${getToken()}`;
  return request;
});

export { axiosInstance };
export default {
  ...config,
};
