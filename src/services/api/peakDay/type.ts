export enum IDirection {
  CAFO = 'CAFO',
  FOCA = 'FOCA',
}

export enum IActionType {
  DELETE = 'DELETE',
  ADD = 'ADD',
  UPDATE = 'UPDATE',
}
export interface IAddPeakInput {
  dates: string[];
  direction: IDirection;
  comment?: string;
}

export interface IPeakDay {
  date: string;
  comment: string;
  direction: IDirection;
}
