import { format } from 'date-fns';
import { omit } from 'lodash';
import { getLanguageCode } from '../../../locales/i18n';
import { axiosInstance } from '../axios';
import { IAddPeakInput, IDirection, IPeakDay } from './type';

export const DIRECTION_OPTIONS: {
  value: IDirection;
  label: string;
}[] = [
  {
    value: IDirection.CAFO,
    label: 'CALAIS - FOLKESTONE',
  },
  {
    value: IDirection.FOCA,
    label: 'FOLKESTONE - CALAIS',
  },
];

export const DIRECTION = {
  CAFO: {
    color: '#FFFAAE',
    label: 'CALAIS - FOLKESTONE',
  },
  FOCA: {
    color: '#0FFEFE',
    label: 'FOLKESTONE - CALAIS',
  },
};

export const exportPeaks = async (options: {
  start?: Date;
  end?: Date;
}): Promise<Blob | undefined> => {
  try {
    const response = await axiosInstance.get(`/peak-days-export`, {
      params: {
        'min-date': options.start && format(options.start, 'yyyy-MM-dd'),
        'max-date': options.end && format(options.end, 'yyyy-MM-dd'),
        lg: getLanguageCode(),
      },
      headers: {
        Accept: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      },
      responseType: 'blob',
    });
    const blob = new Blob([response.data], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    return blob;
  } catch (error) {
    return undefined;
  }
};

export const createPeak = async (peakInput: IAddPeakInput): Promise<IPeakDay[]> => {
  const peaks: IPeakDay[] = (peakInput.dates || []).map(i => ({
    comment: peakInput.comment ?? '',
    date: i,
    direction: peakInput.direction,
  }));

  try {
    const res = await Promise.all(
      peakInput.dates.map(d => {
        const peak: IPeakDay = {
          comment: peakInput.comment ?? '',
          date: format(new Date(d), 'yyyy-MM-dd'),
          direction: peakInput.direction,
        };
        try {
          return axiosInstance.post(`/peak-days`, peak);
        } catch (error) {
          return undefined;
        }
      })
    );
    return res.every(i => i?.status.toString().charAt(0) === '2') ? peaks : [];
  } catch (error) {
    return [];
  }
};

export const updatePeak = async (peak: IPeakDay): Promise<IPeakDay | undefined> => {
  try {
    await axiosInstance.put(`/peak-days`, omit(peak, ['date', 'direction']), {
      params: {
        date: format(new Date(peak.date), 'yyyy-MM-dd'),
        direction: peak.direction,
      },
    });
    return peak;
  } catch (error) {
    return undefined;
  }
};

export const deletePeak = async (peak: IPeakDay): Promise<IPeakDay | undefined> => {
  try {
    await axiosInstance.delete(`/peak-days`, {
      params: {
        date: format(new Date(peak.date), 'yyyy-MM-dd'),
        direction: peak.direction,
      },
    });
    return peak;
  } catch (error) {
    return undefined;
  }
};
export const getPeaks = async (options: { start?: Date; end?: Date }): Promise<IPeakDay[]> => {
  try {
    const response = await axiosInstance.get<any>(`/peak-days`, {
      params: {
        'min-date': options.start && format(options.start, 'yyyy-MM-dd'),
        'max-date': options.end && format(options.end, 'yyyy-MM-dd'),
      },
    });
    return response.data.peakItemList || [];
  } catch (error) {
    return [];
  }
};
