export interface IDefaultFilterParams {
  'page-size'?: string;
  'page-number'?: string;
  filter?: string;
  sort?: string;
}
