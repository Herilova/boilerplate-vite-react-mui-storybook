import { format } from 'date-fns';
import { omit } from 'lodash';
import { getLanguageCode } from '../../../locales/i18n';
import { axiosInstance } from '../axios';
import { IAddStopCashSalesInput, IDirection, IStopCashSales } from './type';

export const DIRECTION_OPTIONS: {
  value: IDirection;
  label: string;
}[] = [
  {
    value: IDirection.CAFO,
    label: 'CALAIS - FOLKESTONE',
  },
  {
    value: IDirection.FOCA,
    label: 'FOLKESTONE - CALAIS',
  },
];

export const DIRECTION = {
  CAFO: {
    color: '#FFFAAE',
    label: 'CALAIS - FOLKESTONE',
  },
  FOCA: {
    color: '#0FFEFE',
    label: 'FOLKESTONE - CALAIS',
  },
};

export const exportStopCashSaless = async (options: {
  start?: Date;
  end?: Date;
}): Promise<Blob | undefined> => {
  try {
    const response = await axiosInstance.get(`/stop-cash-sales-export`, {
      params: {
        'min-date': options.start && format(options.start, 'yyyy-MM-dd'),
        'max-date': options.end && format(options.end, 'yyyy-MM-dd'),
        lg: getLanguageCode(),
      },
      headers: {
        Accept: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      },
      responseType: 'blob',
    });
    const blob = new Blob([response.data], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    return blob;
  } catch (error) {
    return undefined;
  }
};

export const createStopCashSales = async (
  stopCashSalesInput: IAddStopCashSalesInput
): Promise<IStopCashSales[]> => {
  const stopCashSalesList: IStopCashSales[] = (stopCashSalesInput.dates || []).map(i => ({
    comment: stopCashSalesInput.comment ?? '',
    date: i,
    direction: stopCashSalesInput.direction,
  }));

  try {
    const res = await Promise.all(
      stopCashSalesInput.dates.map(d => {
        const stopCashSales: IStopCashSales = {
          comment: stopCashSalesInput.comment ?? '',
          date: format(new Date(d), 'yyyy-MM-dd'),
          direction: stopCashSalesInput.direction,
        };
        try {
          return axiosInstance.post(`/stop-cash-sales`, stopCashSales);
        } catch (error) {
          return undefined;
        }
      })
    );
    return res.every(i => i?.status.toString().charAt(0) === '2') ? stopCashSalesList : [];
  } catch (error) {
    return [];
  }
};

export const updateStopCashSales = async (
  stopCashSales: IStopCashSales
): Promise<IStopCashSales | undefined> => {
  try {
    await axiosInstance.put(`/stop-cash-sales`, omit(stopCashSales, ['date', 'direction']), {
      params: {
        date: format(new Date(stopCashSales.date), 'yyyy-MM-dd'),
        direction: stopCashSales.direction,
      },
    });
    return stopCashSales;
  } catch (error) {
    return undefined;
  }
};

export const deleteStopCashSales = async (
  stopCashSales: IStopCashSales
): Promise<IStopCashSales | undefined> => {
  try {
    await axiosInstance.delete(`/stop-cash-sales`, {
      params: {
        date: format(new Date(stopCashSales.date), 'yyyy-MM-dd'),
        direction: stopCashSales.direction,
      },
    });
    return stopCashSales;
  } catch (error) {
    return undefined;
  }
};
export const getStopCashSaless = async (options: {
  start?: Date;
  end?: Date;
}): Promise<IStopCashSales[]> => {
  try {
    const response = await axiosInstance.get<any>(`/stop-cash-sales`, {
      params: {
        'min-date': options.start && format(options.start, 'yyyy-MM-dd'),
        'max-date': options.end && format(options.end, 'yyyy-MM-dd'),
      },
    });
    return response.data.stopCashSalesItemList || [];
  } catch (error) {
    return [];
  }
};
