export enum IDirection {
  CAFO = 'CAFO',
  FOCA = 'FOCA',
}

export enum IActionType {
  DELETE = 'DELETE',
  ADD = 'ADD',
  UPDATE = 'UPDATE',
}
export interface IAddStopCashSalesInput {
  dates: string[];
  direction: IDirection;
  comment?: string;
}

export interface IStopCashSales {
  date: string;
  comment: string;
  direction: IDirection;
}
