export interface SigninInput {
  user: string;
  pwd: string;
}
