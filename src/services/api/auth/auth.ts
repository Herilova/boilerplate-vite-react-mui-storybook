import { axiosInstance } from '../axios';
import { SigninInput } from './type';

export const signin = (input: SigninInput) => {
  try {
    return axiosInstance.post('/', {
      ...input,
    });
  } catch (error) {
    return undefined;
  }
};
