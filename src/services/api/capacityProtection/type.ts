import { IDefaultFilterParams } from '../type';

export enum IEmailQuotaAction {
  Add = 'Add',
  Delete = 'Delete',
  Update = 'Update',
}
export interface IContactEmailHistories {
  accountName?: string;
  accountNumber?: string;
  mails?: string[];
  action?: IEmailQuotaAction;
  updateDate?: string;
}

export interface IFilterContactEmailHistories extends IDefaultFilterParams {
  'min-date'?: Date;
  'max-date'?: Date;
  'account-number'?: string;
}
export interface IFilterItem {
  id: string;
  value: string;
}

export interface ISortItem {
  id: string;
  desc: boolean;
}

export interface ISellerCodeContact {
  code?: string;
  mail?: string;
  createdBy?: string;
  createdDate?: string;
}

export interface IMailActivation {
  accountNumber?: number;
  isEnabled?: boolean;
  updateDate?: string;
  userName?: string;
}
