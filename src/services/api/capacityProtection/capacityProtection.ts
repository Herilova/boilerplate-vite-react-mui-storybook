import { format } from 'date-fns';
import { getLanguageCode } from '../../../locales/i18n';
import { axiosInstance } from '../axios';
import { IDefaultFilterParams } from '../type';
import {
  IContactEmailHistories,
  IFilterContactEmailHistories,
  IMailActivation,
  ISellerCodeContact,
} from './type';

export const getEmailSellerGroup = async (
  params: Partial<IDefaultFilterParams>
): Promise<ISellerCodeContact[]> => {
  try {
    const response = await axiosInstance.get<{
      getCommercialSellerGroupItem?: ISellerCodeContact[];
    }>(`/seller-code-contact`, {
      params,
    });
    return response.data.getCommercialSellerGroupItem || [];
  } catch (error) {
    return [];
  }
};

export const createEmailSellerGroup = async (
  params: Partial<ISellerCodeContact>
): Promise<ISellerCodeContact | undefined> => {
  try {
    const response = await axiosInstance.post<ISellerCodeContact>(`/seller-code-contact`, {
      code: params.code,
      mail: params.mail,
    });
    return response.data;
  } catch (error) {
    return undefined;
  }
};
export const updateEmailSellerGroup = async (
  params: Partial<ISellerCodeContact>
): Promise<ISellerCodeContact | undefined> => {
  try {
    const response = await axiosInstance.put<ISellerCodeContact>(`/seller-code-contact`, {
      code: params.code,
      mail: params.mail,
    });
    return response.data;
  } catch (error) {
    return undefined;
  }
};

export const deleteEmailSellerGroup = async (
  params: Partial<ISellerCodeContact>
): Promise<ISellerCodeContact | undefined> => {
  try {
    const response = await axiosInstance.delete<ISellerCodeContact>(`/seller-code-contact`, {
      params: {
        code: params.code,
        mail: params.mail,
      },
    });
    return response.data;
  } catch (error) {
    return undefined;
  }
};

export const getEmailContactHistories = async (
  params: IFilterContactEmailHistories
): Promise<IContactEmailHistories[]> => {
  try {
    const response = await axiosInstance.get<{
      getHistoryMailItemList?: IContactEmailHistories[];
    }>(`/capacity-allowance-reached-contacts`, {
      params: {
        ...params,
        'min-date': params['min-date'] && format(params['min-date'], 'yyyy-MM-dd'),
        'max-date': params['max-date'] && format(params['max-date'], 'yyyy-MM-dd'),
      },
    });
    return response.data?.getHistoryMailItemList || [];
  } catch (error) {
    return [];
  }
};

export const getHistoriesActivationEmail = async (
  params: IDefaultFilterParams
): Promise<IMailActivation[]> => {
  try {
    const response = await axiosInstance.get<{
      getActivationEmailItemList?: IMailActivation[];
    }>(`/capacity-allowance-reached-activation/histories`, {
      params,
    });
    return response.data?.getActivationEmailItemList || [];
  } catch (error) {
    return [];
  }
};

export const postActivationEmail = async (o: {
  enable: boolean;
}): Promise<IMailActivation | undefined> => {
  try {
    const response = await axiosInstance.post<IMailActivation>(
      `/capacity-allowance-reached-activation`,
      {
        ...o,
      }
    );
    return response.data;
  } catch (error) {
    return undefined;
  }
};

export const getCurrentActivationEmail = async (): Promise<IMailActivation | undefined> => {
  try {
    const response = await axiosInstance.get<IMailActivation>(
      `/capacity-allowance-reached-activation`
    );
    return response.data;
  } catch (error) {
    return undefined;
  }
};

export const exportSellerCodeContact = async (options: {
  start?: Date;
  end?: Date;
}): Promise<Blob | undefined> => {
  try {
    const response = await axiosInstance.get(`/seller-code-contact-export`, {
      params: {
        'min-date': options.start && format(options.start, 'yyyy-MM-dd'),
        'max-date': options.end && format(options.end, 'yyyy-MM-dd'),
        lg: getLanguageCode(),
      },
      headers: {
        Accept: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      },
      responseType: 'blob',
    });
    const blob = new Blob([response.data], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    return blob;
  } catch (error) {
    return undefined;
  }
};
