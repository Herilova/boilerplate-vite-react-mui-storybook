import {
  BrowserCacheLocation,
  Configuration,
  EndSessionPopupRequest,
  PopupRequest,
  PublicClientApplication,
} from '@azure/msal-browser';
import config from '../../config';

// Config object to be passed to Msal on creation
export const msalConfig: Configuration = {
  auth: {
    clientId: config.msal.clientId,
    authority: `https://login.microsoftonline.com/${config.msal.tenantId}`,
    redirectUri: `${window.location.origin}/dashboard`,
  },
  system: {
    allowNativeBroker: false, // Disables WAM Broker
  },
  cache: {
    cacheLocation: BrowserCacheLocation.LocalStorage,
  },
};

// Add here scopes for id token to be used at MS Identity Platform endpoints.
export const loginRequest: PopupRequest = {
  scopes: ['User.Read'],
  redirectUri: `${window.location.origin}/dashboard`,
};
export const logoutRequest: EndSessionPopupRequest = {
  mainWindowRedirectUri: `${window.location.origin}/logout`,
  postLogoutRedirectUri: `${window.location.origin}/logout`,
};

// Add here the endpoints for MS Graph API services you would like to use.
export const graphConfig = {
  graphMeEndpoint: config.msal.graphMeEndpoint,
};

export const msalInstance = new PublicClientApplication(msalConfig);
