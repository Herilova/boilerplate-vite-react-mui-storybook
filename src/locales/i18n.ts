import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import translationEN from './en.json';
import translationFR from './fr.json';

export type ILanguageCode = 'fr' | 'en';
export const saveLanguageCode = (value: string) => {
  i18n.changeLanguage(value);
  localStorage.setItem('LANGUAGE_CODE', value);
};

export const getLanguageCode = (): ILanguageCode => {
  return (localStorage.getItem('LANGUAGE_CODE') || 'fr') as ILanguageCode;
};

// the translations
const resources = {
  en: {
    translation: translationEN,
  },
  fr: {
    translation: translationFR,
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: getLanguageCode(),
    fallbackLng: 'en',
    keySeparator: false, // we do not use keys in form messages.welcome
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });
export default i18n;
