import { AxiosResponse } from 'axios';
import { ComponentProps, createContext, useContext, useEffect, useState } from 'react';
import { axiosInstance } from '../services/api/axios';

const TOKEN_KEY = 'bearer';
const EXPIRED_KEY = 'expiredAt';
export interface IToken {
  token: string;
  expiredAt: string;
  setToken: (v?: { value: string; expiredAt: string }) => void;
}

export const TokenContext = createContext<IToken>({
  token: '',
  expiredAt: '',
  setToken: () => '',
});

export const TokenContextProvider = ({ children }: ComponentProps<'div'>) => {
  const [token, setToken] = useState('');
  const [expiredAt, setExpiredAt] = useState('');
  useEffect(() => {
    axiosInstance.interceptors.response.use(
      res => {
        return res;
      },
      (err: { response?: AxiosResponse<any> }) => {
        return err?.response;
      }
    );
    setToken(getToken());
    setExpiredAt(window.localStorage.getItem(EXPIRED_KEY) || '');
  }, []);

  function handleSetToken(v?: { value: string; expiredAt: string }) {
    setToken(v?.value || '');
    window.localStorage.setItem(TOKEN_KEY, v?.value || '');
    window.localStorage.setItem(EXPIRED_KEY, v?.expiredAt || '');
  }
  return (
    <TokenContext.Provider
      value={{
        setToken: handleSetToken,
        token,
        expiredAt,
      }}
    >
      {children}
    </TokenContext.Provider>
  );
};

export const getToken = (): string => {
  const expiredAtValue = window.localStorage.getItem(EXPIRED_KEY) || '';
  if (!expiredAtValue || (expiredAtValue && new Date(expiredAtValue) > new Date())) {
    return window.localStorage.getItem(TOKEN_KEY) || '';
  }
  return '';
};

export const useToken = () => useContext(TokenContext);
export const clearToken = () => {
  localStorage.removeItem(TOKEN_KEY);
  localStorage.removeItem(EXPIRED_KEY);
};
