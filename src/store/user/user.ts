import { create } from 'zustand';

interface UserStore {
  user: {
    id: string;
    username: string;
  };
  setUser: (v: UserStore['user']) => void;
}
export const useUserStore = create<UserStore>(set => ({
  user: {
    id: '',
    username: '',
  },
  setUser: (input: UserStore['user']) =>
    set(state => ({
      user: {
        ...state.user,
        ...input,
      },
    })),
}));
