const config = {
  baseUrl: import.meta.env.VITE_BASE_URL,
  msal: {
    clientId: import.meta.env.VITE_CLIENT_ID,
    tenantId: import.meta.env.VITE_TENANT_ID,
    authority: import.meta.env.VITE_AUTORITY,
    graphMeEndpoint: import.meta.env.VITE_GRAPH_ME_ENDPOINT,
  },
};
export default config;
