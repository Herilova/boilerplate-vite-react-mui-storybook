import './index.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { ThemeProvider } from '@emotion/react';
import { CssBaseline } from '@mui/material';
import { SnackbarProvider } from 'notistack';
import React, { useMemo } from 'react';
import ReactDOM from 'react-dom/client';
import { I18nextProvider, useTranslation } from 'react-i18next';
import { QueryClient, QueryClientProvider } from 'react-query';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { TokenContextProvider } from './context/token';
import i18n from './locales/i18n';
import { createTheme } from './themes/theme';

const queryClient = new QueryClient();

const ThemeProviderApp = () => {
  const { i18n } = useTranslation();
  const theme = useMemo(() => createTheme(i18n.language as any), [i18n.language]);
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <SnackbarProvider
        autoHideDuration={4000}
        anchorOrigin={{
          horizontal: 'center',
          vertical: 'bottom',
        }}
      >
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </SnackbarProvider>
    </ThemeProvider>
  );
};

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <I18nextProvider i18n={i18n}>
      <QueryClientProvider client={queryClient}>
        <TokenContextProvider>
          <ThemeProviderApp />
        </TokenContextProvider>
      </QueryClientProvider>
    </I18nextProvider>
  </React.StrictMode>
);
