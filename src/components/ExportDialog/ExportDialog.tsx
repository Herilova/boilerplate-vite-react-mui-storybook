// @ts-ignore
import { Close, DateRangeOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Stack,
  Typography,
} from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers';
import { endOfMonth, format, startOfMonth } from 'date-fns';
import React, { FC, SetStateAction, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation } from 'react-query';

interface IExportDialog {
  open: boolean;
  setOpen: React.Dispatch<SetStateAction<boolean>>;
  fetchFile: (params: Record<string, any>) => Promise<Blob | undefined>;
  title?: string;
}
const ExportDialog: FC<IExportDialog> = props => {
  const { t } = useTranslation();
  const [open, setOpen] = [props.open, props.setOpen];
  const [range, setRange] = useState<{
    start: Date | null;
    end: Date | null;
  }>({
    start: startOfMonth(new Date()),
    end: endOfMonth(new Date()),
  });

  const { mutateAsync: mutateExport, isLoading } = useMutation(props.fetchFile);
  const handleClose = () => {
    setOpen(false);
  };

  const handleExport = () => {
    mutateExport({
      end: range.end || undefined,
      start: range.start || undefined,
    })
      .then(file => {
        if (file) {
          const link = document.createElement('a');
          const url = URL.createObjectURL(file);
          link.href = url;
          link.download = `PEAK_${format(new Date(), 'yyyy-MM-dd HH-mm')}`;
          link.click();
        }
      })
      .finally(() => setOpen(false));
  };

  return (
    <Dialog open={open} maxWidth={false} onClose={handleClose}>
      <DialogContent
        sx={{
          // minWidth: 700,
          width: '600px',
        }}
      >
        <Box display="flex" justifyContent="space-between">
          <span />
          <Typography>{props.title || t('export')}</Typography>
          <IconButton size="small" onClick={handleClose}>
            <Close fontSize="small" />
          </IconButton>
        </Box>
        <Box display="flex" flexDirection="column" rowGap={2} paddingTop={2}>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <DateRangeOutlined />
            <Stack
              direction="row"
              columnGap={1}
              justifyContent="space-between"
              alignItems="center"
              width="100%"
            >
              <DatePicker
                label={t('start_date')}
                value={range.start}
                maxDate={range.end ? range.end : null}
                onChange={start =>
                  setRange(prev => ({
                    ...prev,
                    start,
                  }))
                }
              />
              <DatePicker
                label={t('end_date')}
                value={range.end}
                minDate={range.start ? range.start : null}
                onChange={end =>
                  setRange(prev => ({
                    ...prev,
                    end,
                  }))
                }
              />
            </Stack>
          </Box>
        </Box>
      </DialogContent>
      <DialogActions
        sx={{
          mx: 2,
        }}
      >
        <LoadingButton
          variant="outlined"
          color="primary"
          onClick={handleExport}
          loading={isLoading}
          disabled={!range.end || !range.start}
        >
          {t('export')}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

export default ExportDialog;
