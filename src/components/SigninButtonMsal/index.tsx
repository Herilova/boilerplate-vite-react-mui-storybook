import { useMsal } from '@azure/msal-react';
import { LoadingButton } from '@mui/lab';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { useToken } from '../../context/token';
import { loginRequest } from '../../services/msal/authConfig';

const SignInButton = () => {
  const { t } = useTranslation();
  const { instance } = useMsal();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const { setToken } = useToken();
  const handleLogin = () => {
    setLoading(true);
    instance
      .loginPopup({
        ...loginRequest,
      })
      .then(res => {
        if (res.account) {
          setToken({
            value: res.accessToken,
            expiredAt: res.expiresOn ? res.expiresOn.toISOString() : '',
          });
          navigate('/dashboard');
        }
      })
      .finally(() => setLoading(false));
  };

  return (
    <LoadingButton
      loading={loading}
      onClick={handleLogin}
      color="primary"
      variant="contained"
      size="large"
    >
      {t('sign_in')}
    </LoadingButton>
  );
};
export default SignInButton;
