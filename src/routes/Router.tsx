import { lazy } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';

const Dashboard = lazy(() => import('../page/Dashboard/Dashboard'));
const Login = lazy(() => import('../page/Login'));
const Logout = lazy(() => import('../page/Logout'));

const Router = () => {
  return (
    <Routes>
      <Route path="/logout" element={<Logout />} />
      <Route path="/login" element={<ProtectedRoute access="public" Component={<Login />} />} />
      <Route path="/" element={<Navigate to="/dashboard" />} />
      {/** protected private */}
      <Route
        path="/dashboard/*"
        element={<ProtectedRoute access="private" Component={<Dashboard />} />}
      />
    </Routes>
  );
};

export default Router;
