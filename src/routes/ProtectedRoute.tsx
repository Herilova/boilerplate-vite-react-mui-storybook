import React, { FC, useMemo } from 'react';
import { Navigate } from 'react-router-dom';

type AccessType = 'private' | 'public';

interface ProtectedRouteProps {
  Component: React.ReactElement;
  access?: AccessType;
}
const ProtectedRoute: FC<ProtectedRouteProps> = props => {
  const { Component, access = 'public' } = props;
  // const isAuthenticated = useIsAuthenticated();
  const isAuthenticated = true;
  const redirectionPath = useMemo(() => {
    switch (access) {
      case 'private':
        return isAuthenticated ? undefined : '/login';
      default:
        return !isAuthenticated ? undefined : '/dashboard';
    }
  }, [isAuthenticated, access]);
  if (redirectionPath) {
    return <Navigate to={redirectionPath} />;
  }
  return Component;
};

export default ProtectedRoute;
