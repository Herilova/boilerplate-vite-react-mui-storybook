/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_CLIENT_ID: string;
  readonly VITE_AUTORITY: string;
  readonly VITE_BASE_URL: string;
  readonly VITE_GRAPH_ME_ENDPOINT: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
