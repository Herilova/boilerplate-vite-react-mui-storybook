import { AuthenticationResult, EventType, IPublicClientApplication } from '@azure/msal-browser';
import { MsalProvider } from '@azure/msal-react';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import * as locales from 'date-fns/locale';
import { useSnackbar } from 'notistack';
import { FC, Suspense, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import Loading from './commons/Loading';
import { DialogConfirmContextProvider } from './hooks/useConfirmDialog';
import { Router } from './routes';
import { axiosInstance } from './services/api/axios';
import { msalInstance } from './services/msal/authConfig';
import { CustomNavigationClient } from './utils/navigationClient';

const App: FC = () => {
  const { i18n, t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const [instancePCA, setInstancePCA] = useState<IPublicClientApplication>();
  const navigate = useNavigate();
  const navigationClient = new CustomNavigationClient(navigate);
  useEffect(() => {
    msalInstance.initialize().then(() => {
      // Account selection logic is app dependent. Adjust as needed for different use cases.
      const accounts = msalInstance.getAllAccounts();
      if (accounts.length > 0) {
        msalInstance.setActiveAccount(accounts[0]);
      }

      msalInstance.addEventCallback(event => {
        if (event.eventType === EventType.LOGIN_SUCCESS && event.payload) {
          const payload = event.payload as AuthenticationResult;
          const { account } = payload;
          msalInstance.setActiveAccount(account);
        }
      });
      msalInstance.setNavigationClient(navigationClient);
      setInstancePCA(msalInstance);
    });

    axiosInstance.interceptors.response.use(
      response => response,
      error => {
        if (error != null && error.response != null) {
          let message = t('error_is_occured');
          if (
            error?.response?.message ||
            error?.response?.statusText ||
            error?.response?.data?.title
          ) {
            message =
              error?.response?.message ||
              error?.response?.statusText ||
              error?.response?.data?.title;
          }
          if (message) {
            enqueueSnackbar(message, {
              variant: 'error',
            });
          }
        }
        return Promise.reject(error);
      }
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!instancePCA) return <Loading isFullHeight={true} />;
  return (
    <Suspense fallback={<Loading isFullHeight={true} />}>
      <MsalProvider instance={instancePCA}>
        <LocalizationProvider
          dateAdapter={AdapterDateFns}
          adapterLocale={i18n.language === 'fr' ? locales.fr : locales.enUS}
        >
          <DialogConfirmContextProvider>
            <Router />
          </DialogConfirmContextProvider>
        </LocalizationProvider>
      </MsalProvider>
    </Suspense>
  );
};

export default App;
