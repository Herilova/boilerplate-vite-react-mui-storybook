import { styled } from '@mui/material';

export const ContainerLoading = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'row',
  alignSelf: 'center',
  '&.fullScreen': {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));
