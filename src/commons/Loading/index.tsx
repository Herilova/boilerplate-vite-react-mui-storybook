import { CircularProgress, CircularProgressProps } from '@mui/material';
import { FC, HTMLAttributes } from 'react';
import { ContainerLoading } from './styles';

interface LoadingProps {
  isFullHeight?: boolean;
  size?: number;
  circularProgressProps?: CircularProgressProps;
}

const Loading: FC<LoadingProps & HTMLAttributes<HTMLDivElement>> = props => {
  const { isFullHeight, size, circularProgressProps, ...otherProps } = props;
  return (
    <ContainerLoading {...otherProps} className={isFullHeight ? 'fullScreen' : ''}>
      <CircularProgress {...circularProgressProps} size={size || circularProgressProps?.size} />
    </ContainerLoading>
  );
};

Loading.defaultProps = {
  isFullHeight: false,
  size: undefined,
  circularProgressProps: undefined,
};

export default Loading;
