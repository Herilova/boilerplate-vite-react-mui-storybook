import { Color, styled, Switch as SwitchMui, SwitchProps } from '@mui/material';
import { omit } from 'lodash';

interface ISwitchProps extends SwitchProps {
  activeColor?: Color | string;
  inactiveColor?: Color | string;
}
const Switch = styled((props: ISwitchProps) => (
  <SwitchMui
    focusVisibleClassName=".Mui-focusVisible"
    disableRipple={true}
    {...omit(props, ['activeColor', 'inactiveColor'])}
  />
))(({ theme, activeColor, inactiveColor }) => ({
  width: 42,
  height: 26,
  padding: 0,
  '& .MuiSwitch-switchBase': {
    padding: 0,
    margin: 2,

    top: 0,
    bottom: 0,
    transitionDuration: '300ms',
    '&.Mui-checked': {
      transform: 'translateX(16px)',
      color: '#fff',
      '& + .MuiSwitch-track': {
        backgroundColor: activeColor && activeColor,
        opacity: 1,
        border: 0,
      },
      '&.Mui-disabled + .MuiSwitch-track': {
        opacity: 0.5,
      },
    },
    '&.Mui-disabled .MuiSwitch-thumb': {},
    '&.Mui-disabled + .MuiSwitch-track': {
      opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
    },
  },
  '& .MuiSwitch-thumb': {
    boxSizing: 'border-box',
    // width: 22,
    // height: 22,
  },
  '& .MuiSwitch-track': {
    borderRadius: 26 / 2,
    backgroundColor: inactiveColor && inactiveColor,
    opacity: 1,
    transition: theme.transitions.create(['background-color'], {
      duration: 500,
    }),
  },
}));

export default Switch;
