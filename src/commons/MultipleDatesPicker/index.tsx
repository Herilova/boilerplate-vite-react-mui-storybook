import { Delete } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Box, Dialog, DialogActions, IconButton, Typography } from '@mui/material';
import { DateCalendar, PickersDay, PickersDayProps } from '@mui/x-date-pickers';
import { isEqual, startOfDay } from 'date-fns';
import { orderBy } from 'lodash';
import { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

interface IMultipleDatesPickerProps {
  open: boolean;
  onClose: () => void;
  dates: Date[];
  setDates: (v: Date[]) => void;
  minDate?: Date;
  maxDate?: Date;
}

const MultipleDatesPicker: FC<IMultipleDatesPickerProps> = props => {
  const { onClose, open } = props;
  const [dates, setDates] = useState(props.dates);
  const { t } = useTranslation();

  useEffect(() => {
    setDates(props.dates);
  }, [props.dates]);
  const handleConfirm = () => {
    props.setDates(dates);
    onClose();
  };
  const handleClickDay = (date: Date) => {
    setDates(prev => {
      const exist = prev.find(i => isEqual(startOfDay(i), startOfDay(date)));
      let res = [...prev];
      if (exist) {
        res = prev.filter(i => !isEqual(startOfDay(i), startOfDay(date)));
      } else {
        res = [...prev.map(i => startOfDay(i)), startOfDay(date)];
      }
      return orderBy(res, i => i, 'asc');
    });
  };
  const isSelected = (date: Date) => {
    return Boolean(dates.find(i => isEqual(startOfDay(i), startOfDay(date))));
  };
  return (
    <Dialog open={open} onClose={onClose}>
      <Box display="flex" flexDirection="row">
        <DateCalendar
          slots={{
            day: (props: PickersDayProps<Date>) => (
              <PickersDay {...props} selected={isSelected(props.day)} />
            ),
          }}
          minDate={props.minDate}
          maxDate={props.maxDate}
          onChange={date => {
            if (date) {
              handleClickDay(date);
            }
          }}
        />
        <Box
          p={2}
          sx={{
            maxHeight: 334,
          }}
        >
          <Typography fontWeight="bold">{`${t('selected_date')} ${dates.length}`}</Typography>
          <Box
            sx={{
              overflowY: 'auto',
              height: '100%',
              py: 1,
            }}
          >
            {dates.map(date => (
              <Box
                key={date.toISOString()}
                display="flex"
                flexDirection="row"
                justifyContent="space-between"
                alignItems="center"
              >
                <Typography>{date.toLocaleDateString()}</Typography>
                <IconButton onClick={() => handleClickDay(date)} size="small">
                  <Delete fontSize="small" />
                </IconButton>
              </Box>
            ))}
          </Box>
        </Box>
      </Box>
      <DialogActions
        sx={{
          mt: 1,
        }}
      >
        <LoadingButton variant="outlined" color="primary" onClick={onClose}>
          {t('cancel')}
        </LoadingButton>
        <LoadingButton variant="outlined" color="primary" onClick={handleConfirm}>
          {t('confirm')}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

export default MultipleDatesPicker;
