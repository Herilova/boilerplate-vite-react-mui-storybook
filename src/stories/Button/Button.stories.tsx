import { Button, Stack } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';

type Story = StoryObj<typeof Button>;

export const Base: Story = {
  render: args => <Button {...args}>Base</Button>,
};

export const Variants = () => (
  <Stack spacing={2} maxWidth={300}>
    <Button variant="text">Text Button</Button>
    <Button variant="contained">Contained Button</Button>
    <Button variant="outlined">Outlined Button</Button>
  </Stack>
);

export const Colors = () => (
  <Stack spacing={2} maxWidth={300}>
    <Button variant="contained">Primary</Button>
    <Button variant="contained" color="secondary">
      Secondary
    </Button>
    <Button variant="contained" color="success">
      Success
    </Button>
    <Button variant="contained" color="error">
      Text Button
    </Button>
  </Stack>
);

export const Sizes = () => (
  <Stack spacing={2} maxWidth={300}>
    <Button variant="contained" size="small">
      Small
    </Button>
    <Button variant="contained" size="medium">
      Medium
    </Button>
    <Button variant="contained" size="large">
      Large
    </Button>
  </Stack>
);

export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    variant: {
      options: ['contained', 'outlined', 'text'],
      name: 'variant',
      defaultValue: 'contained',
      description: 'Variant of button',
      table: {
        type: {
          summary: 'string',
        },
        defaultValue: {
          summary: 'contained',
        },
      },
      control: {
        type: 'select',
      },
    },
    color: {
      options: ['primary', 'secondary', 'success', 'error'],
      name: 'color',
      defaultValue: 'primary',
      description: 'Color of button',
      table: {
        type: {
          summary: 'string',
        },
        defaultValue: {
          summary: 'primary',
        },
      },
      control: {
        type: 'select',
      },
    },
    children: {
      name: 'Title',
      description: 'Text of button',
      defaultValue: 'Button',
      table: {
        type: {
          summary: 'children',
        },
      },
      control: {
        type: 'text',
      },
    },
  },
} as Meta<typeof Button>;
