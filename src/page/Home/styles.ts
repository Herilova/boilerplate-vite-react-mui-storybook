import { styled } from '@mui/material';

export const ContainerHome = styled('div')(() => ({
  width: '100vw',
  height: '100vh',
  overflow: 'hidden',
}));
export const ContainerHomeBody = styled('div')(() => ({
  flex: 1,
}));
