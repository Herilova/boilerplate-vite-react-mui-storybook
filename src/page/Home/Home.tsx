import { Box, Typography } from '@mui/material';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import useMaslAccount from '../../hooks/useMaslAccount';

const Home: FC = () => {
  const account = useMaslAccount();
  const { t } = useTranslation();
  return (
    <Box
      width="100%"
      height="100%"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <Typography variant="h5">
        {t('hello_user', {
          name: account?.name || account?.username || '',
        })}
      </Typography>
      <Typography variant="h6">{t('welcom_msg')}</Typography>
    </Box>
  );
};

export default Home;
