import { Box, FormControlLabel, Stack, Typography, useTheme } from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { format } from 'date-fns';
import { FC, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from 'react-query';
import { v4 } from 'uuid';
import Switch from '../../../commons/Switch';
import useDatagrid from '../../../hooks/useDatagrid';
import {
  getCurrentActivationEmail,
  getHistoriesActivationEmail,
  postActivationEmail,
} from '../../../services/api/capacityProtection/capacityProtection';
import { IMailActivation } from '../../../services/api/capacityProtection/type';

interface IMailActivationData extends IMailActivation {
  id: string;
}
const MailActivation: FC = () => {
  const theme = useTheme();
  const { t } = useTranslation();
  const { dataGridProps, sortParams, paginationParams, calculateMaxRowCount } = useDatagrid();
  const [sendMailInactifQuota, setSendMailInactifQuota] = useState(false);

  const { data: dataCurrentActivation, refetch } = useQuery(
    'current-activation',
    getCurrentActivationEmail,
    { enabled: false }
  );
  const {
    data,
    isLoading,
    refetch: refetchList,
  } = useQuery(
    ['email-activation', sortParams, paginationParams],
    async () => {
      const res = await getHistoriesActivationEmail({
        sort: sortParams,
        ...paginationParams,
      });
      calculateMaxRowCount(res);
      return res;
    },
    {
      enabled: false,
    }
  );
  const dataUniq = useMemo(() => (data || []).map(i => ({ ...i, id: v4() })), [data]);

  useEffect(() => {
    refetchList();
  }, [paginationParams, sortParams, refetchList]);
  useEffect(() => {
    refetch();
  }, [refetch]);
  useEffect(() => {
    setSendMailInactifQuota(Boolean(dataCurrentActivation?.isEnabled));
  }, [dataCurrentActivation?.isEnabled]);

  const columns: GridColDef<IMailActivationData>[] = [
    {
      field: 'userName',
      headerName: t('user'),
      width: 130,
      editable: false,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params => `${params.row?.userName || ''}`,
    },
    {
      field: 'isEnabled',
      headerName: t('action'),
      width: 130,
      editable: false,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params =>
        params.row.isEnabled ? t(params.row.isEnabled ? 'enabled' : 'disabled') : '',
    },
    {
      field: 'updateDate',
      headerName: t('date'),
      width: 260,
      editable: false,
      filterable: false,
      sortable: false,
      disableColumnMenu: true,
      sortComparator: (v1, v2) => (new Date(v1) > new Date(v2) ? 1 : -1),
      valueGetter: params =>
        params.row.updateDate
          ? `${new Date(params.row.updateDate).toLocaleDateString()} ${format(
              new Date(params.row.updateDate),
              'HH:mm:ss'
            )}`
          : '',
    },
  ];
  const handleChangeReachedAction = async (checked: boolean) => {
    setSendMailInactifQuota(checked);
    postActivationEmail({
      enable: checked,
    });
    await refetchList();
    await refetch();
  };
  return (
    <Box>
      <Stack rowGap={3}>
        <Stack rowGap={1}>
          <Typography fontWeight="bold">{`${t('send_mail_quota_state_desc')} :`}</Typography>
          <FormControlLabel
            sx={{
              ml: '0px',
            }}
            control={
              <Switch
                checked={sendMailInactifQuota}
                color="error"
                activeColor={theme.palette.success.main}
                inactiveColor={theme.palette.error.main}
              />
            }
            label={
              <Typography ml={1}>{t(sendMailInactifQuota ? 'enabled' : 'disabled')}</Typography>
            }
            onChange={(_, checked) => handleChangeReachedAction(checked)}
          />
        </Stack>
        <Stack rowGap={1}>
          <Typography fontWeight="bold">{`${t('historic_mail_quota_state_desc')}`}</Typography>
          <DataGrid
            sx={{
              backgroundColor: '#FFFFFF',
            }}
            rows={dataUniq}
            columns={columns}
            rowSelection={false}
            getRowId={row => row.id}
            loading={isLoading}
            autoHeight={true}
            {...dataGridProps}
          />
        </Stack>
      </Stack>
    </Box>
  );
};

export default MailActivation;
