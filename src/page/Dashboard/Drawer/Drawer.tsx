import { ChevronRight, ExpandLess, ExpandMore } from '@mui/icons-material';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import {
  Box,
  Collapse,
  List,
  ListItemButtonProps,
  ListItemIcon,
  ListItemText,
  Tooltip,
  Typography,
} from '@mui/material';
import IconButton from '@mui/material/IconButton';
import { FC, MouseEvent, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate } from 'react-router-dom';
import dashboardRoutes, { IRoute } from '../dashboardRoutes';
import { Dot, Drawer as DrawerStyled, ListItemButton, MenuPopover } from './style';

interface IDrawer {
  open: boolean;
  handleToogleDrawer: () => void;
}

export const ListElement: FC<
  {
    route: IRoute;
    parent?: IRoute;
    openMenu?: boolean;
    isInMenuPopover?: boolean;
  } & ListItemButtonProps
> = props => {
  const { isInMenuPopover, route, openMenu, parent, ...otherProps } = props;
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [open, setOpen] = useState(false);

  const isParentMenu = useMemo(() => {
    return Boolean(route.childs && route.childs.length);
  }, [route]);
  const disableTooltip = useMemo(() => openMenu || isInMenuPopover, [openMenu, isInMenuPopover]);

  useEffect(() => {
    if (!openMenu) {
      setOpen(false);
    }
  }, [openMenu]);
  const handleClick = (event: MouseEvent<HTMLElement>) => {
    if (props.onClick) {
      props.onClick(event as any);
    }
    setAnchorEl(event.currentTarget);
    if (isParentMenu) {
      setOpen(prev => !prev);
    } else {
      navigate(`${parent ? `${parent.path}/` : ''}${route.path}`);
    }
  };
  return (
    <>
      <Tooltip
        title={t(route.label)}
        disableHoverListener={disableTooltip}
        disableFocusListener={disableTooltip}
        disableInteractive={disableTooltip}
        disableTouchListener={disableTooltip}
        placement="right-start"
      >
        <ListItemButton
          {...otherProps}
          key={route.label}
          onClick={handleClick}
          selected={
            (!isParentMenu || (isParentMenu && !open && !openMenu)) &&
            pathname.includes(`${route.path}`)
          }
        >
          {route.icon && (
            <ListItemIcon
              sx={{
                color: 'currentColor',
                minWidth: 38,
              }}
            >
              <route.icon />
            </ListItemIcon>
          )}

          {(openMenu || isInMenuPopover || (isParentMenu && !isInMenuPopover && !openMenu)) && (
            <ListItemText
              primary={
                isParentMenu && !isInMenuPopover && !openMenu ? (
                  <Dot
                    style={{
                      marginLeft: -10,
                    }}
                  />
                ) : (
                  <Typography noWrap={true}>{t(route.label)}</Typography>
                )
              }
            />
          )}
          {isParentMenu && openMenu && (open ? <ExpandLess /> : <ExpandMore />)}
          {/* {isParentMenu && !isInMenuPopover && !openMenu && <Dot />} */}
        </ListItemButton>
      </Tooltip>
      {isParentMenu &&
        (!openMenu && !isInMenuPopover ? (
          <MenuPopover
            open={open}
            onClose={() => setOpen(false)}
            anchorEl={anchorEl}
            anchorOrigin={{
              horizontal: 'right',
              vertical: 'center',
            }}
          >
            {route.childs?.map(child => (
              <ListElement
                key={route.label}
                route={child}
                parent={route}
                sx={{ pl: 4, minWidth: 200 }}
                openMenu={false}
                isInMenuPopover={true}
                onClick={() => setOpen(false)}
              />
            ))}
          </MenuPopover>
        ) : (
          <Collapse in={open} timeout="auto" unmountOnExit={true}>
            <List component="div" disablePadding={true}>
              {route.childs?.map(child => (
                <ListElement
                  {...otherProps}
                  key={route.label}
                  route={child}
                  parent={route}
                  sx={{ pl: 4 }}
                  openMenu={openMenu}
                  isInMenuPopover={isInMenuPopover}
                />
              ))}
            </List>
          </Collapse>
        ))}
    </>
  );
};
const Drawer = ({ handleToogleDrawer, open }: IDrawer) => {
  return (
    <DrawerStyled variant="permanent" open={open}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: open ? 'flex-end' : 'center',
          py: 1,
        }}
      >
        <IconButton
          style={{
            outline: 'none',
          }}
          onClick={handleToogleDrawer}
          size="small"
          color="info"
        >
          {open ? (
            <ChevronLeftIcon
              fontSize="small"
              sx={{
                color: '#FFFFFF',
              }}
            />
          ) : (
            <ChevronRight
              fontSize="small"
              sx={{
                color: '#FFFFFF',
              }}
            />
          )}
        </IconButton>
      </Box>
      <List component="nav">
        {dashboardRoutes.map(route => (
          <ListElement key={route.label} route={route} openMenu={open} />
        ))}
      </List>
    </DrawerStyled>
  );
};

export default Drawer;
