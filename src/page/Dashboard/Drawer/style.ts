import { Menu } from '@mui/material';
import MuiDrawer from '@mui/material/Drawer';
import MuiListItemButton from '@mui/material/ListItemButton';
import { styled } from '@mui/material/styles';
import { APPBAR_HEIGHT } from '../AppBar/style';
import { drawerWidth, drawerWidthMini } from '../style';

export const MenuPopover = styled(Menu)(() => ({
  '& .MuiPaper-root': {
    backgroundColor: '#0e0f0f',
  },
  '& .MuiList-root': {
    padding: '0px',
  },
}));

export const Dot = styled('span')(() => ({
  width: 5,
  height: 5,
  backgroundColor: 'currentcolor',
  borderRadius: '50%',
  display: 'inline-block',
}));
export const Drawer = styled(MuiDrawer, { shouldForwardProp: prop => prop !== 'open' })(
  ({ open }) => ({
    height: '100vh',
    '& .MuiDrawer-paper': {
      backgroundColor: '#0e0f0f',
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      height: `calc(100vh - ${APPBAR_HEIGHT}px)`,
      top: APPBAR_HEIGHT,
      // transition: theme.transitions.create('width', {
      //   easing: theme.transitions.easing.sharp,
      //   duration: theme.transitions.duration.enteringScreen,
      // }),
      boxSizing: 'border-box',
      ...(!open && {
        overflowX: 'hidden',
        // transition: theme.transitions.create('width', {
        //   easing: theme.transitions.easing.sharp,
        //   duration: theme.transitions.duration.leavingScreen,
        // }),
        width: drawerWidthMini,
      }),
    },
  })
);

export const ListItemButton = styled(MuiListItemButton)(() => ({
  color: '#FFFFFF',
  minHeight: 48,
  paddingRight: 5,
  '&.Mui-selected': {
    backgroundColor: '#15201c',
    color: '#7fe4b8',
  },
}));
