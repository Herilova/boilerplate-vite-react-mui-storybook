import { CalendarToday, PointOfSale, Security } from '@mui/icons-material';
import React from 'react';
import ComingSoonPage from './ComingSoonPage/ComingSoonPage';
import MailActivation from './MailActivation/MailActivation';
import MailQuota from './MailQuota/MailQuota';
import MailSellerGroup from './MailSellerGroup/MailSellerGroup';
import PeakDay from './PeakDay/PeakDay';
import StopCashSales from './StopCashSales/StopCashSales';

export interface IRoute {
  path: string;
  component?: React.FC;
  label: string;
  icon?: React.FC;
  childs?: IRoute[];
}
const dashboardRoutes: IRoute[] = [
  {
    path: 'peak-day',
    component: PeakDay,
    label: 'peak_day',
    icon: CalendarToday,
  },
  {
    path: 'stop-cash-sales',
    component: StopCashSales,
    label: 'stop_cash_sales',
    icon: PointOfSale,
  },
  {
    path: 'capacity-protection',
    label: 'capacity_protection',
    icon: Security,
    childs: [
      {
        path: 'activation-mail',
        component: MailActivation,
        label: 'send_email_out_of_quot',
      },
      {
        path: 'capacity-allowance-reached',
        component: MailQuota,
        label: 'email_modification_history',
      },
      {
        path: 'seller-code-contact',
        component: MailSellerGroup,
        label: 'seller_group_email',
      },
      {
        path: 'webservice-management',
        component: ComingSoonPage,
        label: 'web_service_management',
      },
      {
        path: 'automating',
        component: ComingSoonPage,
        label: 'automating',
      },
      // {
      //   path: 'other',
      //   component: CapacityProtection,
      //   label: 'Autre',
      // },
    ],
  },
];

export default dashboardRoutes;
