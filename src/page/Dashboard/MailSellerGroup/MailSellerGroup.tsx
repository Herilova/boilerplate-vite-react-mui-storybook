import { Add, Delete, Send } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Box, Stack, TextField, Typography } from '@mui/material';
import { DataGrid, GridActionsCellItem, GridColDef } from '@mui/x-data-grid';
import { FC, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { TiExport } from 'react-icons/ti';
import { useQuery } from 'react-query';
import { v4 } from 'uuid';
import ExportDialog from '../../../components/ExportDialog/ExportDialog';
import useDatagrid from '../../../hooks/useDatagrid';
import {
  exportSellerCodeContact,
  getEmailSellerGroup,
} from '../../../services/api/capacityProtection/capacityProtection';
import { IFilterItem, ISellerCodeContact } from '../../../services/api/capacityProtection/type';
import MailSellerGroupDialog from './MailSellerGroupDialog';

interface ISellerCodeContactData extends ISellerCodeContact {
  id: string;
}
const MailSellerGroup: FC = () => {
  const { t } = useTranslation();
  const [filter, setFilter] = useState<
    Partial<{
      code?: string;
    }>
  >({});
  const [mode, setMode] = useState<'edit' | 'create'>('create');

  const { dataGridProps, sortParams, paginationParams, calculateMaxRowCount } = useDatagrid();
  const [form, setForm] = useState<ISellerCodeContact | undefined>();
  const [openExport, setOpenExport] = useState(false);

  const { data, isLoading, refetch } = useQuery(
    ['email-seller-group', sortParams, paginationParams],
    async () => {
      const filterParams: IFilterItem[] = [];
      if (filter.code) {
        filterParams.push({
          id: 'code',
          value: filter.code,
        });
      }
      const res = await getEmailSellerGroup({
        filter: filterParams.length ? JSON.stringify(filterParams) : undefined,
        sort: sortParams,
        ...paginationParams,
      });
      calculateMaxRowCount(res);
      return res;
    },
    {
      enabled: false,
    }
  );
  const dataUniq = useMemo(
    () =>
      (data || []).map(i => ({
        ...i,
        id: v4(),
      })),
    [data]
  );

  useEffect(() => {
    refetch();
  }, [paginationParams, refetch, sortParams]);

  const columns: GridColDef<ISellerCodeContactData>[] = [
    {
      field: 'code',
      headerName: t('seller_code'),
      width: 130,
      editable: false,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params => `${params.row.code || ''}`,
    },
    {
      field: 'mail',
      headerName: t('email_address'),
      editable: false,
      width: 230,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params => `${params.row.mail || ''}`,
    },
    {
      field: 'createdBy',
      headerName: t('user'),
      width: 150,
      type: 'string',
      editable: false,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params => `${params.row.createdBy || ''}`,
    },
    {
      field: 'action',
      headerName: '',
      width: 150,
      sortable: false,
      type: 'actions',
      getActions: p => {
        return [
          <GridActionsCellItem
            key="delete"
            icon={<Delete />}
            label={t('delete')}
            onClick={() => {
              setMode('edit');
              setForm({
                ...p.row,
              });
            }}
            color="inherit"
          />,
        ];
      },
    },
  ];
  const handleChangeFilter = <K extends keyof typeof filter>(key: K, value: (typeof filter)[K]) => {
    setFilter(prev => ({
      ...prev,
      [key]: value,
    }));
  };
  return (
    <Box
      style={{
        overflowX: 'auto',
      }}
    >
      <Stack rowGap={3}>
        <Stack rowGap={1}>
          <Typography fontWeight="bold">{`${t('select_search_date')}`}</Typography>
          <Stack
            style={{
              display: 'grid',
              gridTemplateColumns: 'repeat(auto-fit, minmax(200px, 1fr))',
              gap: '10px',
            }}
          >
            <TextField
              label={t('seller_code')}
              value={filter.code}
              onChange={e => {
                handleChangeFilter('code', e.target.value);
              }}
              size="small"
            />
          </Stack>
          <Stack display="flex" flexDirection="row" justifyContent="center">
            <LoadingButton
              variant="contained"
              color="primary"
              loading={isLoading}
              onClick={() => refetch()}
              endIcon={<Send />}
            >
              {t('send')}
            </LoadingButton>
          </Stack>
        </Stack>
        <Stack rowGap={1}>
          <Typography fontWeight="bold">{`${t(
            'list_of_emails_associated_with_seller_groups'
          )}`}</Typography>
          <DataGrid
            sx={{
              backgroundColor: '#FFFFFF',
            }}
            rows={dataUniq}
            columns={columns}
            rowSelection={false}
            loading={isLoading}
            autoHeight={true}
            {...dataGridProps}
          />
          <Stack display="flex" flexDirection="row" justifyContent="space-between">
            <LoadingButton
              variant="outlined"
              onClick={() => setOpenExport(true)}
              startIcon={<TiExport />}
            >
              {t('export_excel')}
            </LoadingButton>
            <LoadingButton
              variant="outlined"
              onClick={() => {
                setMode('create');
                setForm({
                  mail: '',
                  code: '',
                  createdBy: '',
                  createdDate: new Date().toISOString(),
                });
              }}
              startIcon={<Add />}
            >
              {t('add_a_contact')}
            </LoadingButton>
          </Stack>
        </Stack>
      </Stack>
      {form && (
        <MailSellerGroupDialog
          mode={mode}
          setData={setForm}
          data={form}
          onCompleted={() => refetch()}
        />
      )}
      <ExportDialog fetchFile={exportSellerCodeContact} open={openExport} setOpen={setOpenExport} />
    </Box>
  );
};

export default MailSellerGroup;
