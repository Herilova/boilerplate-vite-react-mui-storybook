import { Close } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Typography,
} from '@mui/material';
import React, { FC, SetStateAction, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation } from 'react-query';
import useConfirmDialog from '../../../hooks/useConfirmDialog';
import {
  createEmailSellerGroup,
  deleteEmailSellerGroup,
  updateEmailSellerGroup,
} from '../../../services/api/capacityProtection/capacityProtection';
import { ISellerCodeContact } from '../../../services/api/capacityProtection/type';

interface IMailSellerGroupDialog {
  data?: ISellerCodeContact;
  setData: React.Dispatch<SetStateAction<ISellerCodeContact | undefined>>;
  onCompleted: (newValue: ISellerCodeContact) => void;
  mode: 'edit' | 'create';
}

const MailSellerGroupDialog: FC<IMailSellerGroupDialog> = props => {
  const { t } = useTranslation();
  const confirmDialog = useConfirmDialog();

  const [data, setData] = useState<Partial<ISellerCodeContact>>({});

  const { mutateAsync: mutateSave, isLoading: isLoadingSave } = useMutation(createEmailSellerGroup);
  const { mutateAsync: mutateDelete, isLoading: isLoadingDelete } =
    useMutation(deleteEmailSellerGroup);
  const { mutateAsync: mutateUpdate, isLoading: isLoadingUpdate } =
    useMutation(updateEmailSellerGroup);

  useEffect(() => {
    if (props.data) setData(props.data);
  }, [props.data]);

  const handleClose = () => {
    props.setData(undefined);
  };

  const handleSave = async () => {
    const { mail } = data;
    if (mail) {
      if (props.mode === 'create') {
        mutateSave({
          code: data?.code,
          mail,
        }).then(response => {
          if (response) {
            props.onCompleted(data as any);
          }
          handleClose();
        });
      } else {
        mutateUpdate({
          code: data?.code,
          mail,
        }).then(response => {
          if (response) {
            props.onCompleted(data as any);
          }
          handleClose();
        });
      }
    }
  };
  const handleDelete = async () => {
    const { mail } = data;
    if (mail) {
      const confirm = await confirmDialog.confirm(
        <Typography>{t('confirm_action_delete')}</Typography>
      );
      if (confirm) {
        mutateDelete({
          ...data,
        }).then(resp => {
          if (resp) {
            props.onCompleted(data as any);
          }
          handleClose();
        });
      }
    }
  };
  if (!data) return <div />;
  return (
    <Dialog open={Boolean(props.data)} maxWidth={false} onClose={handleClose}>
      <DialogContent
        sx={{
          // minWidth: 700,
          width: '600px',
        }}
      >
        <Box display="flex" justifyContent="space-between">
          <span />
          <span />
          <IconButton size="small" onClick={handleClose}>
            <Close fontSize="small" />
          </IconButton>
        </Box>
        <Box display="flex" flexDirection="column" rowGap={2} paddingTop={2}>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <TextField
              onChange={evt =>
                setData(prev => ({
                  ...prev,
                  code: evt.target.value,
                }))
              }
              disabled={props.mode === 'edit'}
              size="medium"
              label={t('seller_code')}
              value={data.code ?? ''}
              fullWidth={true}
            />
          </Box>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <TextField
              onChange={evt =>
                setData(prev => ({
                  ...prev,
                  mail: evt.target.value,
                }))
              }
              disabled={props.mode === 'edit'}
              size="medium"
              type="email"
              label={t('email_address')}
              value={data.mail ?? ''}
              fullWidth={true}
            />
          </Box>
        </Box>
      </DialogContent>
      <DialogActions
        sx={{
          mx: 2,
        }}
      >
        {props.mode === 'edit' && (
          <LoadingButton
            variant="outlined"
            color="warning"
            onClick={handleDelete}
            loading={isLoadingDelete}
          >
            {t('delete')}
          </LoadingButton>
        )}
        <LoadingButton
          variant="outlined"
          color="primary"
          loading={isLoadingSave || isLoadingUpdate}
          onClick={handleSave}
          disabled={!data.mail || !data.code}
        >
          {t('save')}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

export default MailSellerGroupDialog;
