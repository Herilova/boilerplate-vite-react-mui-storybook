import './fullCalendar.css';
import { DateSelectArg, EventClickArg, EventContentArg, EventInput } from '@fullcalendar/core';
import allLocales from '@fullcalendar/core/locales-all';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import FullCalendar from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';
import { Add } from '@mui/icons-material';
import { Box, Fab, useTheme } from '@mui/material';
import {
  addDays,
  eachDayOfInterval,
  endOfDay,
  isPast,
  isSameDay,
  startOfDay,
  subDays,
} from 'date-fns';
import { useSnackbar } from 'notistack';
import { FC, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ExportDialog from '../../../components/ExportDialog/ExportDialog';
import {
  DIRECTION,
  DIRECTION_OPTIONS,
  exportPeaks,
  getPeaks,
} from '../../../services/api/peakDay/peakDay';
import {
  IActionType,
  IAddPeakInput,
  IDirection,
  IPeakDay,
} from '../../../services/api/peakDay/type';
import PeakDayDialog from './PeakDayDialog';

const defaultPeak: IPeakDay = {
  direction: IDirection.CAFO,
  date: new Date().toISOString(),
  comment: '',
};

const PeakDay: FC = () => {
  const theme = useTheme();
  const { enqueueSnackbar } = useSnackbar();
  const { t, i18n } = useTranslation();

  const [weekendsVisible] = useState(true);
  const [range, setRange] = useState<{
    start?: Date;
    end?: Date;
  }>({});
  const [editPeak, setEditPeak] = useState<IAddPeakInput | undefined>();
  const [directionOptions, setDirectionOptions] = useState(DIRECTION_OPTIONS);
  const [peaks, setPeaks] = useState<IPeakDay[]>([]);
  const [exportDialogOpen, setExportDialogOpen] = useState(false);
  const [mode, setMode] = useState<'edit' | 'create'>('create');

  const currentEvents = useMemo(() => {
    const res: EventInput[] = peaks.map(i => {
      const isPastDay = isPast(addDays(new Date(i.date), 1));
      return {
        id: `${i.date}-${i.direction}`,
        allDay: true,
        start: i.date,
        title: i.direction ? DIRECTION[i.direction].label : '',
        backgroundColor:
          i.direction === IDirection.CAFO
            ? theme.palette.primary.main
            : theme.palette.secondary.main,
        textColor:
          i.direction === IDirection.CAFO
            ? theme.palette.primary.contrastText
            : theme.palette.secondary.contrastText,
        borderColor: 'transparent',
        editable: !isPastDay,
      };
    });
    return res;
  }, [peaks, theme.palette]);

  useEffect(() => {
    if (range.end && range.start) {
      getPeaks({
        end: range.end,
        start: range.start,
      }).then(res => {
        setPeaks(res);
      });
    }
  }, [range]);

  const isSamePeak = (peak1: IPeakDay, peak2: IPeakDay) => {
    return `${peak1.date}-${peak1.direction}` === `${peak2.date}-${peak2.direction}`;
  };
  const afterSubmit = (values: IPeakDay[], type: IActionType) => {
    let message = '';
    if (type === IActionType.DELETE) {
      message = t('delete_done');
      setPeaks(prev => prev.filter(i => !values.some(l => isSamePeak(l, i))));
    } else {
      message = type === IActionType.UPDATE ? t('update_done') : t('save_done');
      setPeaks(prev => [...prev.filter(i => !values.some(l => isSamePeak(l, i))), ...values]);
    }
    setDirectionOptions(DIRECTION_OPTIONS);
    setMode('create');
    enqueueSnackbar(message, {
      variant: 'success',
    });
  };
  const handleCreateDay = (date: Date) => {
    setDirectionOptions(DIRECTION_OPTIONS);
    const peakDays = peaks.filter(i => isSameDay(new Date(i.date), date));
    if (peakDays.length === 2) return;
    if (!isPast(addDays(new Date(date), 1))) {
      const findPeak = peaks.find(i => isSameDay(new Date(i.date), date));
      const eachDays = eachDayOfInterval(
        {
          start: startOfDay(date),
          end: endOfDay(date),
        },
        {
          step: 1,
        }
      );
      setMode('create');
      setDirectionOptions(prev =>
        defaultPeak.direction === findPeak?.direction
          ? prev.filter(i => i.value === IDirection.FOCA)
          : prev
      );
      setEditPeak({
        direction:
          defaultPeak.direction === findPeak?.direction ? IDirection.FOCA : defaultPeak.direction,
        comment: '',
        dates: eachDays.map(i => startOfDay(i).toISOString()),
      });
    }
  };
  const handleDateSelect = (selectInfo: DateSelectArg) => {
    const calendarApi = selectInfo.view.calendar;
    calendarApi.unselect();
    const peak = selectInfo;
    const { start } = peak;
    const end = peak.end ? subDays(new Date(peak.end), 1) : peak.start;
    if (start && end) {
      handleCreateDay(new Date(start));
    }
  };

  const handleEventClick = (clickInfo: EventClickArg) => {
    const { id, start } = clickInfo.event;
    if (!start) return;
    const peak = peaks.find(i => id === `${i.date}-${i.direction}`);
    setDirectionOptions(DIRECTION_OPTIONS);
    if (peak?.date && !isPast(addDays(new Date(peak.date), 1))) {
      setMode('edit');
      setEditPeak({
        dates: [peak.date],
        comment: peak.comment,
        direction: peak.direction,
      });
    }
  };

  const renderEventContent = (eventContent: EventContentArg) => {
    const { id } = eventContent.event;
    const peak = currentEvents.find(i => id && i.id === id);
    return (
      <i
        style={{
          whiteSpace: 'normal',
        }}
      >
        {peak?.direction?.destination && peak.direction.origin
          ? `${peak.direction.origin} - ${peak.direction.destination}`
          : peak?.title}
      </i>
    );
  };
  return (
    <Box height="100%" minWidth="500px">
      <PeakDayDialog
        setPeak={setEditPeak}
        peak={editPeak}
        onCompleted={afterSubmit}
        mode={mode}
        directionOptions={directionOptions}
      />
      <ExportDialog fetchFile={exportPeaks} setOpen={setExportDialogOpen} open={exportDialogOpen} />
      <FullCalendar
        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
        headerToolbar={{
          right: 'title',
          // right: 'dayGridMonth,timeGridWeek,timeGridDay',
          left: 'prev,next today export',
        }}
        dayCellClassNames={e => (e.isPast ? 'disableDay' : '')}
        initialView="dayGridMonth"
        height="100%"
        editable={false}
        selectable={true}
        selectMirror={false}
        dragScroll={false}
        eventResizableFromStart={false}
        eventStartEditable={false}
        eventDurationEditable={false}
        eventOverlap={false}
        droppable={false}
        locales={allLocales}
        weekends={weekendsVisible}
        events={currentEvents}
        locale={i18n.language}
        select={handleDateSelect}
        eventContent={renderEventContent} // custom render function
        eventClick={handleEventClick}
        eventDragMinDistance={999}
        datesSet={e => {
          setRange({
            start: startOfDay(e.start),
            end: endOfDay(e.end),
          });
        }}
        customButtons={{
          export: {
            text: t('export'),
            click: () => {
              setExportDialogOpen(true);
            },
          },
        }}
      />
      <Fab
        size="large"
        color="secondary"
        sx={{
          position: 'fixed',
          bottom: 30,
          right: 30,
        }}
        onClick={() => {
          handleCreateDay(new Date());
        }}
      >
        <Add color="inherit" />
      </Fab>
    </Box>
  );
};

export default PeakDay;
