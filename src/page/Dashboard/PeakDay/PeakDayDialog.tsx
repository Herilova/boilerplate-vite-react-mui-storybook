import { Close, DateRangeOutlined, LocationOn, Notes } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import {
  Autocomplete,
  Box,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import React, { FC, SetStateAction, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import MultipleDatesPicker from '../../../commons/MultipleDatesPicker';
import { useConfirmDialog } from '../../../hooks/useConfirmDialog';
import {
  createPeak,
  deletePeak,
  DIRECTION_OPTIONS,
  updatePeak,
} from '../../../services/api/peakDay/peakDay';
import {
  IActionType,
  IAddPeakInput,
  IDirection,
  IPeakDay,
} from '../../../services/api/peakDay/type';

interface IAddPeakInputDayDialog {
  peak?: IAddPeakInput;
  setPeak: React.Dispatch<SetStateAction<IAddPeakInput | undefined>>;
  onCompleted: (newValue: IPeakDay[], type: IActionType) => void;
  mode: 'edit' | 'create';
  directionOptions: typeof DIRECTION_OPTIONS;
}
const PeakDayDialog: FC<IAddPeakInputDayDialog> = props => {
  const { t } = useTranslation();
  const confirmDialog = useConfirmDialog();
  const [open, setOpen] = useState(false);
  const [peak, setPeak] = useState<IAddPeakInput>({
    dates: [new Date().toISOString()],
    direction: IDirection.CAFO,
    comment: '',
  });

  const directionValue = useMemo(() => {
    const res = (
      peak.direction ? props.directionOptions.find(i => i.value === peak?.direction) : null
    ) as any;
    return res ?? null;
  }, [peak.direction, props.directionOptions]);

  useEffect(() => {
    if (props.peak) setPeak(props.peak);
  }, [props.peak]);

  const handleClose = () => {
    props.setPeak(undefined);
  };

  const handleSave = () => {
    if (peak?.dates?.length) {
      if (props.mode === 'create') {
        createPeak({
          dates: peak.dates,
          comment: peak.comment,
          direction: peak.direction,
        }).then(resp => {
          if (resp.length) {
            props.onCompleted(resp, IActionType.ADD);
          }
          handleClose();
        });
      } else {
        updatePeak({
          comment: peak.comment ?? '',
          direction: peak.direction,
          date: peak.dates[0],
        }).then(resp => {
          if (resp) {
            props.onCompleted([resp], IActionType.UPDATE);
          }
          handleClose();
        });
      }
    }
  };
  const handleDelete = async () => {
    if (peak?.dates?.length) {
      const confirm = await confirmDialog.confirm(
        <Typography>{t('confirm_action_delete')}</Typography>
      );
      if (confirm) {
        const date = peak.dates[0];
        deletePeak({
          date,
          direction: peak.direction,
          comment: peak.comment ?? '',
        }).then(resp => {
          if (resp) {
            props.onCompleted([resp], IActionType.DELETE);
          }
          handleClose();
        });
      }
    }
  };
  if (!peak) return <div />;
  return (
    <Dialog open={Boolean(props.peak)} maxWidth={false} onClose={handleClose}>
      <DialogContent
        sx={{
          // minWidth: 700,
          width: '600px',
        }}
      >
        <Box display="flex" justifyContent="space-between">
          <span />
          <Typography>{t('peak_day')}</Typography>
          <IconButton size="small" onClick={handleClose}>
            <Close fontSize="small" />
          </IconButton>
        </Box>
        <Box display="flex" flexDirection="column" rowGap={2} paddingTop={2}>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <DateRangeOutlined />
            <Stack direction="row" justifyContent="space-between" alignItems="center" width="100%">
              <Stack direction="row" rowGap={1} columnGap={1} flexWrap="wrap">
                {(peak.dates || []).map(date => (
                  <Chip
                    key={date}
                    disabled={props.mode === 'edit'}
                    label={new Date(date).toLocaleDateString()}
                    onDelete={() => {
                      setPeak(prev => ({
                        ...prev,
                        dates: (prev.dates || []).filter(i => i !== date),
                      }));
                    }}
                  />
                ))}
              </Stack>
              <IconButton onClick={() => setOpen(true)} disabled={props.mode === 'edit'}>
                <DateRangeOutlined />
              </IconButton>
              <MultipleDatesPicker
                open={open}
                dates={(peak.dates || []).map(i => new Date(i))}
                onClose={() => setOpen(false)}
                setDates={(dates: Date[]) => {
                  setPeak(prev => ({
                    ...prev,
                    dates: (dates || []).map(i => new Date(i).toISOString()),
                  }));
                  setOpen(false);
                }}
                minDate={new Date()}
              />
            </Stack>
          </Box>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <LocationOn />
            <Autocomplete
              fullWidth={true}
              disabled={props.mode === 'edit'}
              disableClearable={true}
              options={props.directionOptions}
              getOptionLabel={i => i.label}
              value={directionValue}
              onChange={(_, v) => {
                setPeak(prev => ({
                  ...prev,
                  direction: v?.value || undefined,
                }));
              }}
              renderInput={inputProps => (
                <TextField {...inputProps} size="medium" label={t('direction')} fullWidth={true} />
              )}
            />
          </Box>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <Notes />
            <TextField
              onChange={evt =>
                setPeak(prev => ({
                  ...prev,
                  comment: evt.target.value,
                }))
              }
              multiline={true}
              rows={5}
              size="medium"
              label={t('comment')}
              value={peak.comment ?? ''}
              fullWidth={true}
            />
          </Box>
        </Box>
      </DialogContent>
      <DialogActions
        sx={{
          mx: 2,
        }}
      >
        {props.mode === 'edit' && (
          <LoadingButton variant="outlined" color="warning" onClick={handleDelete}>
            {t('delete')}
          </LoadingButton>
        )}
        <LoadingButton
          variant="outlined"
          color="primary"
          onClick={handleSave}
          disabled={!peak.dates?.length || !peak.direction}
        >
          {t('save')}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

export default PeakDayDialog;
