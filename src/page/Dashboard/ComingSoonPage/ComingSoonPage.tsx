import { Box, Typography } from '@mui/material';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

const ComingSoonPage: FC = () => {
  const { t } = useTranslation();
  return (
    <Box
      width="100%"
      height="100%"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <Typography variant="h5">{t('page_coming_soon')}</Typography>
    </Box>
  );
};

export default ComingSoonPage;
