import './fullCalendar.css';
import { DateSelectArg, EventClickArg, EventContentArg, EventInput } from '@fullcalendar/core';
import allLocales from '@fullcalendar/core/locales-all';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import FullCalendar from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';
import { Add } from '@mui/icons-material';
import { Box, Fab, useTheme } from '@mui/material';
import {
  addDays,
  eachDayOfInterval,
  endOfDay,
  isPast,
  isSameDay,
  startOfDay,
  subDays,
} from 'date-fns';
import { useSnackbar } from 'notistack';
import { FC, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ExportDialog from '../../../components/ExportDialog/ExportDialog';
import {
  DIRECTION,
  DIRECTION_OPTIONS,
  exportStopCashSaless,
  getStopCashSaless,
} from '../../../services/api/stopCashSales/stopCashSales';
import {
  IActionType,
  IAddStopCashSalesInput,
  IDirection,
  IStopCashSales,
} from '../../../services/api/stopCashSales/type';
import StopCashSalesDialog from './StopCashSalesDialog';

const defaultStopCashSale: IStopCashSales = {
  direction: IDirection.CAFO,
  date: new Date().toISOString(),
  comment: '',
};

const StopCashSales: FC = () => {
  const theme = useTheme();
  const { enqueueSnackbar } = useSnackbar();
  const { t, i18n } = useTranslation();

  const [weekendsVisible] = useState(true);
  const [range, setRange] = useState<{
    start?: Date;
    end?: Date;
  }>({});
  const [editStopCashSale, setEditStopCashSale] = useState<IAddStopCashSalesInput | undefined>();
  const [directionOptions, setDirectionOptions] = useState(DIRECTION_OPTIONS);
  const [stopCashSalesList, setStopCashSales] = useState<IStopCashSales[]>([]);
  const [exportDialogOpen, setExportDialogOpen] = useState(false);
  const [mode, setMode] = useState<'edit' | 'create'>('create');

  const currentEvents = useMemo(() => {
    const res: EventInput[] = stopCashSalesList.map(i => {
      const isPastDay = isPast(addDays(new Date(i.date), 1));
      return {
        id: `${i.date}-${i.direction}`,
        allDay: true,
        start: i.date,
        title: i.direction ? DIRECTION[i.direction].label : '',
        backgroundColor:
          i.direction === IDirection.CAFO
            ? theme.palette.primary.main
            : theme.palette.secondary.main,
        textColor:
          i.direction === IDirection.CAFO
            ? theme.palette.primary.contrastText
            : theme.palette.secondary.contrastText,
        borderColor: 'transparent',
        editable: !isPastDay,
      };
    });
    return res;
  }, [stopCashSalesList, theme.palette]);

  useEffect(() => {
    if (range.end && range.start) {
      getStopCashSaless({
        end: range.end,
        start: range.start,
      }).then(res => {
        setStopCashSales(res);
      });
    }
  }, [range]);

  const isSameStopCashSale = (stopCashSale1: IStopCashSales, stopCashSale2: IStopCashSales) => {
    return (
      `${stopCashSale1.date}-${stopCashSale1.direction}` ===
      `${stopCashSale2.date}-${stopCashSale2.direction}`
    );
  };
  const afterSubmit = (values: IStopCashSales[], type: IActionType) => {
    let message = '';
    if (type === IActionType.DELETE) {
      message = t('delete_done');
      setStopCashSales(prev => prev.filter(i => !values.some(l => isSameStopCashSale(l, i))));
    } else {
      message = type === IActionType.UPDATE ? t('update_done') : t('save_done');
      setStopCashSales(prev => [
        ...prev.filter(i => !values.some(l => isSameStopCashSale(l, i))),
        ...values,
      ]);
    }
    setDirectionOptions(DIRECTION_OPTIONS);
    setMode('create');
    enqueueSnackbar(message, {
      variant: 'success',
    });
  };
  const handleCreateDay = (date: Date) => {
    setDirectionOptions(DIRECTION_OPTIONS);
    const stopCashSales = stopCashSalesList.filter(i => isSameDay(new Date(i.date), date));
    if (stopCashSales.length === 2) return;
    if (!isPast(addDays(new Date(date), 1))) {
      const findStopCashSale = stopCashSalesList.find(i => isSameDay(new Date(i.date), date));
      const eachDays = eachDayOfInterval(
        {
          start: startOfDay(date),
          end: endOfDay(date),
        },
        {
          step: 1,
        }
      );
      setMode('create');
      setDirectionOptions(prev =>
        defaultStopCashSale.direction === findStopCashSale?.direction
          ? prev.filter(i => i.value === IDirection.FOCA)
          : prev
      );
      setEditStopCashSale({
        direction:
          defaultStopCashSale.direction === findStopCashSale?.direction
            ? IDirection.FOCA
            : defaultStopCashSale.direction,
        comment: '',
        dates: eachDays.map(i => startOfDay(i).toISOString()),
      });
    }
  };
  const handleDateSelect = (selectInfo: DateSelectArg) => {
    const calendarApi = selectInfo.view.calendar;
    calendarApi.unselect();
    const stopCashSale = selectInfo;
    const { start } = stopCashSale;
    const end = stopCashSale.end ? subDays(new Date(stopCashSale.end), 1) : stopCashSale.start;
    if (start && end) {
      handleCreateDay(new Date(start));
    }
  };

  const handleEventClick = (clickInfo: EventClickArg) => {
    const { id, start } = clickInfo.event;
    if (!start) return;
    const stopCashSale = stopCashSalesList.find(i => id === `${i.date}-${i.direction}`);
    setDirectionOptions(DIRECTION_OPTIONS);
    if (stopCashSale?.date && !isPast(addDays(new Date(stopCashSale.date), 1))) {
      setMode('edit');
      setEditStopCashSale({
        dates: [stopCashSale.date],
        comment: stopCashSale.comment,
        direction: stopCashSale.direction,
      });
    }
  };

  const renderEventContent = (eventContent: EventContentArg) => {
    const { id } = eventContent.event;
    const stopCashSale = currentEvents.find(i => id && i.id === id);
    return (
      <i
        style={{
          whiteSpace: 'normal',
        }}
      >
        {stopCashSale?.direction?.destination && stopCashSale.direction.origin
          ? `${stopCashSale.direction.origin} - ${stopCashSale.direction.destination}`
          : stopCashSale?.title}
      </i>
    );
  };
  return (
    <Box height="100%" minWidth="500px">
      <StopCashSalesDialog
        setStopCashSales={setEditStopCashSale}
        stopCashSales={editStopCashSale}
        onCompleted={afterSubmit}
        mode={mode}
        directionOptions={directionOptions}
      />
      <ExportDialog
        fetchFile={exportStopCashSaless}
        setOpen={setExportDialogOpen}
        open={exportDialogOpen}
      />
      <FullCalendar
        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
        headerToolbar={{
          right: 'title',
          // right: 'dayGridMonth,timeGridWeek,timeGridDay',
          left: 'prev,next today export',
        }}
        dayCellClassNames={e => (e.isPast ? 'disableDay' : '')}
        initialView="dayGridMonth"
        height="100%"
        editable={false}
        selectable={true}
        selectMirror={false}
        dragScroll={false}
        eventResizableFromStart={false}
        eventStartEditable={false}
        eventDurationEditable={false}
        eventOverlap={false}
        droppable={false}
        locales={allLocales}
        weekends={weekendsVisible}
        events={currentEvents}
        locale={i18n.language}
        select={handleDateSelect}
        eventContent={renderEventContent} // custom render function
        eventClick={handleEventClick}
        eventDragMinDistance={999}
        datesSet={e => {
          setRange({
            start: startOfDay(e.start),
            end: endOfDay(e.end),
          });
        }}
        customButtons={{
          export: {
            text: t('export'),
            click: () => {
              setExportDialogOpen(true);
            },
          },
        }}
      />
      <Fab
        size="large"
        color="secondary"
        sx={{
          position: 'fixed',
          bottom: 30,
          right: 30,
        }}
        onClick={() => {
          handleCreateDay(new Date());
        }}
      >
        <Add color="inherit" />
      </Fab>
    </Box>
  );
};

export default StopCashSales;
