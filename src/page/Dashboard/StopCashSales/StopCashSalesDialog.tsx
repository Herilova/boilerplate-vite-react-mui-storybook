import { Close, DateRangeOutlined, LocationOn, Notes } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import {
  Autocomplete,
  Box,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import React, { FC, SetStateAction, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import MultipleDatesPicker from '../../../commons/MultipleDatesPicker';
import { useConfirmDialog } from '../../../hooks/useConfirmDialog';
import {
  createStopCashSales,
  deleteStopCashSales,
  DIRECTION_OPTIONS,
  updateStopCashSales,
} from '../../../services/api/stopCashSales/stopCashSales';
import {
  IActionType,
  IAddStopCashSalesInput,
  IDirection,
  IStopCashSales,
} from '../../../services/api/stopCashSales/type';

interface IAddStopCashSalesInputDayDialog {
  stopCashSales?: IAddStopCashSalesInput;
  setStopCashSales: React.Dispatch<SetStateAction<IAddStopCashSalesInput | undefined>>;
  onCompleted: (newValue: IStopCashSales[], type: IActionType) => void;
  mode: 'edit' | 'create';
  directionOptions: typeof DIRECTION_OPTIONS;
}
const StopCashSalesDialog: FC<IAddStopCashSalesInputDayDialog> = props => {
  const { t } = useTranslation();
  const confirmDialog = useConfirmDialog();
  const [open, setOpen] = useState(false);
  const [stopCashSales, setStopCashSales] = useState<IAddStopCashSalesInput>({
    dates: [new Date().toISOString()],
    direction: IDirection.CAFO,
    comment: '',
  });

  const directionValue = useMemo(() => {
    const res = (
      stopCashSales.direction
        ? props.directionOptions.find(i => i.value === stopCashSales?.direction)
        : null
    ) as any;
    return res ?? null;
  }, [stopCashSales.direction, props.directionOptions]);

  useEffect(() => {
    if (props.stopCashSales) setStopCashSales(props.stopCashSales);
  }, [props.stopCashSales]);

  const handleClose = () => {
    props.setStopCashSales(undefined);
  };

  const handleSave = () => {
    if (stopCashSales?.dates?.length) {
      if (props.mode === 'create') {
        createStopCashSales({
          dates: stopCashSales.dates,
          comment: stopCashSales.comment,
          direction: stopCashSales.direction,
        }).then(resp => {
          if (resp.length) {
            props.onCompleted(resp, IActionType.ADD);
          }
          handleClose();
        });
      } else {
        updateStopCashSales({
          comment: stopCashSales.comment ?? '',
          direction: stopCashSales.direction,
          date: stopCashSales.dates[0],
        }).then(resp => {
          if (resp) {
            props.onCompleted([resp], IActionType.UPDATE);
          }
          handleClose();
        });
      }
    }
  };
  const handleDelete = async () => {
    if (stopCashSales?.dates?.length) {
      const confirm = await confirmDialog.confirm(
        <Typography>{t('confirm_action_delete')}</Typography>
      );
      if (confirm) {
        const date = stopCashSales.dates[0];
        deleteStopCashSales({
          date,
          direction: stopCashSales.direction,
          comment: stopCashSales.comment ?? '',
        }).then(resp => {
          if (resp) {
            props.onCompleted([resp], IActionType.DELETE);
          }
          handleClose();
        });
      }
    }
  };
  if (!stopCashSales) return <div />;
  return (
    <Dialog open={Boolean(props.stopCashSales)} maxWidth={false} onClose={handleClose}>
      <DialogContent
        sx={{
          // minWidth: 700,
          width: '600px',
        }}
      >
        <Box display="flex" justifyContent="space-between">
          <span />
          <Typography>{t('stopCashSales_day')}</Typography>
          <IconButton size="small" onClick={handleClose}>
            <Close fontSize="small" />
          </IconButton>
        </Box>
        <Box display="flex" flexDirection="column" rowGap={2} paddingTop={2}>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <DateRangeOutlined />
            <Stack direction="row" justifyContent="space-between" alignItems="center" width="100%">
              <Stack direction="row" rowGap={1} columnGap={1} flexWrap="wrap">
                {(stopCashSales.dates || []).map(date => (
                  <Chip
                    key={date}
                    disabled={props.mode === 'edit'}
                    label={new Date(date).toLocaleDateString()}
                    onDelete={() => {
                      setStopCashSales(prev => ({
                        ...prev,
                        dates: (prev.dates || []).filter(i => i !== date),
                      }));
                    }}
                  />
                ))}
              </Stack>
              <IconButton onClick={() => setOpen(true)} disabled={props.mode === 'edit'}>
                <DateRangeOutlined />
              </IconButton>
              <MultipleDatesPicker
                open={open}
                dates={(stopCashSales.dates || []).map(i => new Date(i))}
                onClose={() => setOpen(false)}
                setDates={(dates: Date[]) => {
                  setStopCashSales(prev => ({
                    ...prev,
                    dates: (dates || []).map(i => new Date(i).toISOString()),
                  }));
                  setOpen(false);
                }}
                minDate={new Date()}
              />
            </Stack>
          </Box>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <LocationOn />
            <Autocomplete
              fullWidth={true}
              disabled={props.mode === 'edit'}
              disableClearable={true}
              options={props.directionOptions}
              getOptionLabel={i => i.label}
              value={directionValue}
              onChange={(_, v) => {
                setStopCashSales(prev => ({
                  ...prev,
                  direction: v?.value || undefined,
                }));
              }}
              renderInput={inputProps => (
                <TextField {...inputProps} size="medium" label={t('direction')} fullWidth={true} />
              )}
            />
          </Box>
          <Box display="flex" flexDirection="row" alignItems="center" columnGap={3}>
            <Notes />
            <TextField
              onChange={evt =>
                setStopCashSales(prev => ({
                  ...prev,
                  comment: evt.target.value,
                }))
              }
              multiline={true}
              rows={5}
              size="medium"
              label={t('comment')}
              value={stopCashSales.comment ?? ''}
              fullWidth={true}
            />
          </Box>
        </Box>
      </DialogContent>
      <DialogActions
        sx={{
          mx: 2,
        }}
      >
        {props.mode === 'edit' && (
          <LoadingButton variant="outlined" color="warning" onClick={handleDelete}>
            {t('delete')}
          </LoadingButton>
        )}
        <LoadingButton
          variant="outlined"
          color="primary"
          onClick={handleSave}
          disabled={!stopCashSales.dates?.length || !stopCashSales.direction}
        >
          {t('save')}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

export default StopCashSalesDialog;
