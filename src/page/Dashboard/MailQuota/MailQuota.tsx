import { Send } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Box, Stack, TextField, Typography } from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { DatePicker } from '@mui/x-date-pickers';
import { format } from 'date-fns';
import { FC, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from 'react-query';
import { v4 } from 'uuid';
import useDatagrid from '../../../hooks/useDatagrid';
import { getEmailContactHistories } from '../../../services/api/capacityProtection/capacityProtection';
import {
  IContactEmailHistories,
  IFilterContactEmailHistories,
} from '../../../services/api/capacityProtection/type';

interface IContactEmailHistoriesData extends IContactEmailHistories {
  id: string;
}
const MailQuota: FC = () => {
  const { t } = useTranslation();
  const [filter, setFilter] = useState<IFilterContactEmailHistories>({});
  const { dataGridProps, sortParams, paginationParams, calculateMaxRowCount } = useDatagrid();

  const { data, isLoading, refetch } = useQuery(
    ['email-quota', sortParams, paginationParams],
    async () => {
      const res = await getEmailContactHistories({
        ...filter,
        sort: sortParams,
        ...paginationParams,
      });
      calculateMaxRowCount(res);
      return res;
    },
    {
      enabled: false,
    }
  );
  const dataUniq = useMemo(
    () =>
      (data || []).map(i => ({
        ...i,
        id: v4(),
      })),
    [data]
  );

  useEffect(() => {
    refetch();
  }, [paginationParams, refetch, sortParams]);

  const columns: GridColDef<IContactEmailHistoriesData>[] = [
    {
      field: 'accountNumber',
      headerName: 'N°',
      width: 130,
      editable: false,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params => `${params.row.accountNumber || ''}`,
    },
    {
      field: 'accountName',
      headerName: t('account_name'),
      width: 150,
      editable: false,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params => `${params.row.accountName || ''}`,
    },
    {
      field: 'mails',
      headerName: t('email_address_s'),
      editable: false,
      width: 230,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params => (params?.row?.mails || []).join(','),
    },
    {
      field: 'action',
      headerName: t('action'),
      width: 150,
      editable: false,
      sortable: false,
      filterable: false,
      disableColumnMenu: true,
      valueGetter: params =>
        params.row.action ? t(params.row.action.toLowerCase()).toUpperCase() : '',
    },
    {
      field: 'updateDate',
      headerName: t('date'),
      width: 260,
      editable: false,
      filterable: false,
      disableColumnMenu: true,
      sortComparator: (v1, v2) => (new Date(v1) > new Date(v2) ? 1 : -1),
      valueGetter: params => {
        if (!params.row.updateDate) return '';
        const date = new Date(params.row.updateDate);
        return `${date.toLocaleDateString()} ${format(date, 'HH:mm:ss')}`;
      },
    },
  ];
  const handleChangeFilter = <K extends keyof typeof filter>(key: K, value: (typeof filter)[K]) => {
    setFilter(prev => ({
      ...prev,
      [key]: value,
    }));
  };
  return (
    <Box
      style={{
        overflowX: 'auto',
      }}
    >
      <Stack rowGap={3}>
        <Stack rowGap={1}>
          <Typography fontWeight="bold">{`${t('select_search_date')}`}</Typography>
          <Stack
            style={{
              display: 'grid',
              gridTemplateColumns: 'repeat(auto-fit, minmax(200px, 1fr))',
              gap: '10px',
            }}
          >
            <DatePicker
              label={t('start_date')}
              value={filter['min-date']}
              maxDate={filter['max-date'] ? filter['max-date'] : null}
              onChange={startDate => handleChangeFilter('min-date', startDate || undefined)}
              slotProps={{
                textField: {
                  size: 'small',
                },
              }}
            />
            <DatePicker
              label={t('end_date')}
              value={filter['max-date']}
              minDate={filter['min-date'] ? filter['min-date'] : null}
              onChange={endDate => handleChangeFilter('max-date', endDate || undefined)}
              slotProps={{
                textField: {
                  size: 'small',
                },
              }}
            />
            <TextField
              label={t('account_number')}
              value={filter['account-number']}
              onChange={e => {
                handleChangeFilter('account-number', e.target.value);
              }}
              size="small"
            />
            {/* <TextField
              label={t('account_name')}
              value={filter.accountName}
              onChange={e => {
                handleChangeFilter('accountName', e.target.value);
              }}
              size="small"
            />
            <Autocomplete
              disableClearable={true}
              options={['all', 'adding', 'updating', 'deleting']}
              value={filter.action ? filter.action : 'all'}
              getOptionLabel={i => t(i)}
              onChange={(_, v) => {
                handleChangeFilter('action', v === 'all' ? undefined : (v as IEmailQuotaAction));
              }}
              renderInput={inputProps => (
                <TextField {...inputProps} fullWidth={true} size="small" />
              )}
            /> */}
          </Stack>
          <Stack display="flex" flexDirection="row" justifyContent="center">
            <LoadingButton
              variant="contained"
              color="primary"
              loading={isLoading}
              onClick={() => refetch()}
              endIcon={<Send />}
            >
              {t('send')}
            </LoadingButton>
          </Stack>
        </Stack>
        <Stack rowGap={1}>
          <Typography fontWeight="bold">{`${t('list_of_email_quota_modification')}`}</Typography>
          <DataGrid
            sx={{
              backgroundColor: '#FFFFFF',
            }}
            rows={dataUniq || []}
            columns={columns}
            rowSelection={false}
            loading={isLoading}
            autoHeight={true}
            {...dataGridProps}
          />
        </Stack>
      </Stack>
    </Box>
  );
};

export default MailQuota;
