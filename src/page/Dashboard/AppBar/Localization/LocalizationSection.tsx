import {
  Avatar,
  Box,
  IconButton,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Menu,
} from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { getLanguageCode, ILanguageCode, saveLanguageCode } from '../../../../locales/i18n';
import supportedLanguage from './language';

const LocalizationSection = () => {
  const { t } = useTranslation();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const [language, setLanguage] = useState<ILanguageCode>(getLanguageCode());

  const handleListItemClick = (lng: ILanguageCode) => {
    setLanguage(lng);
    saveLanguageCode(lng);
    handleClose();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  return (
    <>
      <Box
        sx={{
          ml: '5px',
        }}
      >
        <IconButton size="medium" onClick={handleOpen} color="inherit">
          <Avatar
            src={supportedLanguage[language].logo}
            sx={{
              width: '22px',
              height: '22px',
            }}
          />
        </IconButton>
      </Box>
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted={true}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {Object.keys(supportedLanguage).map(i => (
          <ListItemButton key={i} onClick={() => handleListItemClick(i as any)}>
            <ListItemIcon>
              <Avatar
                src={supportedLanguage[i as ILanguageCode].logo}
                sx={{
                  width: '35px',
                  height: '20px',
                }}
                variant="square"
              />
            </ListItemIcon>
            <ListItemText primary={t(i)} />
          </ListItemButton>
        ))}
      </Menu>
    </>
  );
};

export default LocalizationSection;
