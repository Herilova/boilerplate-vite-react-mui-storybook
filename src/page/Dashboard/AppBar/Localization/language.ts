import en from '../../../../assets/flags/en.png';
import fr from '../../../../assets/flags/fr.png';

const supportedLanguage = {
  fr: {
    label: 'fr',
    logo: fr,
  },
  en: {
    label: 'en',
    logo: en,
  },
};

export default supportedLanguage;
