import { AccountCircle } from '@mui/icons-material';
import { Box, Menu, MenuItem } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { MouseEvent, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import Logo from '../../../commons/Logo';
import useMaslAccount from '../../../hooks/useMaslAccount';
import LocalizationSection from './Localization/LocalizationSection';
import { AppBar as AppBarStyled } from './style';

interface IAppBar {
  open: boolean;
}

const AppBar = ({ open }: IAppBar) => {
  const { t } = useTranslation();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const account = useMaslAccount();
  const navigate = useNavigate();
  const handleMenu = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    // clearToken();
    navigate('/logout');
  };
  return (
    <AppBarStyled position="absolute" open={open}>
      <Toolbar
        sx={{
          pr: '24px', // keep right padding when drawer closed
        }}
      >
        <Box sx={{ flexGrow: 1 }}>
          <Logo height="65" theme="light" />
        </Box>
        <Box>
          <Typography
            component="h6"
            variant="h6"
            color="inherit"
            noWrap={true}
            sx={{ flexGrow: 1 }}
          >
            {account?.name || account?.username || 'User'}
          </Typography>
        </Box>
        <LocalizationSection />
        <div>
          <IconButton
            size="medium"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            keepMounted={true}
            transformOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleLogout}>{t('sign_out')}</MenuItem>
          </Menu>
        </div>
      </Toolbar>
    </AppBarStyled>
  );
};

export default AppBar;
