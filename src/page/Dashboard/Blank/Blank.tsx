import { FC } from 'react';

interface IBlankProps {
  title?: string;
}
const Blank: FC<IBlankProps> = props => {
  return <span>{props.title || ''}</span>;
};

export default Blank;
