import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import { Fragment, useEffect, useMemo, useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import useMobile from '../../hooks/useMobile';
import Home from '../Home';
import AppBar from './AppBar/AppBar';
import { APPBAR_HEIGHT } from './AppBar/style';
import dashboardRoutes from './dashboardRoutes';
import Drawer from './Drawer/Drawer';
import { drawerWidth, drawerWidthMini } from './style';

const Dashboard = () => {
  const [open, setOpen] = useState(true);
  const isMobile = useMobile();
  const allRoutes = useMemo(() => dashboardRoutes, []);
  useEffect(() => {
    setOpen(!isMobile);
  }, [isMobile]);
  const toggleDrawer = () => {
    setOpen(!open);
  };
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar open={open} />
      <Drawer handleToogleDrawer={toggleDrawer} open={open} />
      <Box
        component="main"
        sx={{
          backgroundColor: theme =>
            theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900],
          position: 'absolute',
          left: open ? drawerWidth : drawerWidthMini,
          top: APPBAR_HEIGHT,
          width: `calc(100vw - ${open ? drawerWidth : drawerWidthMini}px)`,
          height: `calc(100vh - ${APPBAR_HEIGHT}px)`,
          overflow: 'auto',
          p: 2,
        }}
      >
        <Routes>
          <Route path="/" element={<Home />} />
          {allRoutes.map(route => (
            <Fragment key={route.path}>
              <Route path={`/${route.path}`} Component={route.component} key={route.path}>
                {(route.childs || []).map(child => (
                  <Route path={child.path} Component={child.component} key={route.path} />
                ))}
              </Route>
            </Fragment>
          ))}
        </Routes>
      </Box>
    </Box>
  );
};

export default Dashboard;
