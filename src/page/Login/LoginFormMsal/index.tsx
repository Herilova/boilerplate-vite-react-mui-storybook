import { Stack } from '@mui/material';
import { FC } from 'react';
import SignInButton from '../../../components/SigninButtonMsal';
import { ContainerLoginForm } from './styles';

const LoginFormMsal: FC = () => {
  return (
    <ContainerLoginForm>
      <Stack rowGap={2} width="100%">
        <SignInButton />
      </Stack>
    </ContainerLoginForm>
  );
};

export default LoginFormMsal;
