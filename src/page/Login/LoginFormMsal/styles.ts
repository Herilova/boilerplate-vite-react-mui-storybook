import { styled } from '@mui/material';

export const ContainerLoginForm = styled('div')(() => ({
  maxWidth: 400,
  margin: 'auto',
  marginTop: 50,
}));
