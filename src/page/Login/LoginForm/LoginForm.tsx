import { LoadingButton } from '@mui/lab';
import { Stack, TextField } from '@mui/material';
import { FC, KeyboardEvent, useState } from 'react';
import { useToken } from '../../../context/token';
import { ContainerLoginForm } from './styles';

const LoginForm: FC = () => {
  const { setToken } = useToken();
  const [username, setUsername] = useState<string | null>('');
  const [password, setPassword] = useState<string | null>('');

  const handleLogin = () => {
    if (username?.trim() && password?.length) {
      setToken({
        value: username,
        expiredAt: '',
      });
      window.location.href = '/dashboard';
    }
  };

  const handleEnterPress = (evt: KeyboardEvent<HTMLDivElement>) => {
    if (evt.key === 'Enter') {
      handleLogin();
    }
  };

  return (
    <ContainerLoginForm onKeyDown={handleEnterPress}>
      <form onSubmit={handleLogin}>
        <Stack rowGap={2} width="100%">
          <TextField
            onChange={evt => setUsername(evt.target.value || null)}
            value={username || ''}
            error={username === undefined}
            placeholder="Email"
            fullWidth={true}
          />
          <TextField
            onChange={evt => setPassword(evt.target.value || null)}
            value={password || ''}
            error={password === undefined}
            type="password"
            placeholder="Mot de passe"
            autoComplete=""
            fullWidth={true}
          />
          <LoadingButton
            onClick={handleLogin}
            variant="contained"
            color="primary"
            disabled={!password || !username}
            fullWidth={true}
          >
            Connexion
          </LoadingButton>
        </Stack>
      </form>
    </ContainerLoginForm>
  );
};

export default LoginForm;
