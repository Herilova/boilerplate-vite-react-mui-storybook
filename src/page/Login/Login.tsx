import { InteractionRequiredAuthError, InteractionType } from '@azure/msal-browser';
import { useMsalAuthentication } from '@azure/msal-react';
import { Box, CircularProgress, Grid, Typography } from '@mui/material';
import { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { loginRequest } from '../../services/msal/authConfig';
import LoginFormMsal from './LoginFormMsal';
import { CenterPanel, LoginPageContainer, PanelContainer } from './styles';

const Login: FC = () => {
  const { t } = useTranslation();
  const [showManualSignIn, setShowManualSignIn] = useState(false);
  const { login, error } = useMsalAuthentication(InteractionType.Silent, loginRequest);

  useEffect(() => {
    if (error instanceof InteractionRequiredAuthError) {
      login(InteractionType.Popup, loginRequest);
    } else if (error) {
      setShowManualSignIn(true);
    }
  }, [error, login]);

  return (
    <LoginPageContainer>
      <PanelContainer container={true} lg={8} item={true} sm={12}>
        <Grid container={true}>
          <CenterPanel item={true} sm={12} xs={12}>
            {!showManualSignIn && (
              <Box display="flex" flexDirection="column" alignItems="center" rowGap={2}>
                <CircularProgress />
                <Typography color="black" align="center" variant="h3" fontWeight={600}>
                  {t('connexion_in_progress')}
                </Typography>
              </Box>
            )}
            {showManualSignIn && (
              <>
                <Typography color="black" align="center" variant="h3" fontWeight={600}>
                  {t('sign_in')}
                </Typography>
                <LoginFormMsal />
              </>
            )}
          </CenterPanel>
        </Grid>
      </PanelContainer>
    </LoginPageContainer>
  );
};

export default Login;
