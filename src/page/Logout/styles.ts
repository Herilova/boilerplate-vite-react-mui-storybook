import { Grid, styled } from '@mui/material';

export const LoginPageContainer = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  minHeight: '100vh',
  width: '100vw',
}));
export const PanelContainer = styled(Grid)(() => ({
  maxWidth: 1291,
}));

export const CenterPanel = styled(Grid)(({ theme }) => ({
  height: 654,
  position: 'relative',
  [theme.breakpoints.down('xs')]: {
    height: 'auto',
    paddingBottom: 35,
  },
  padding: '86px 120px',
  [theme.breakpoints.down('sm')]: {
    padding: '35px 35px 35px 35px',
  },
}));
