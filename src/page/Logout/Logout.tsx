import { useIsAuthenticated } from '@azure/msal-react';
import { CheckOutlined } from '@mui/icons-material';
import { Box, CircularProgress, Grid, Typography } from '@mui/material';
import { FC, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { msalInstance } from '../../services/msal/authConfig';
import { CenterPanel, LoginPageContainer, PanelContainer } from './styles';

const Logout: FC = () => {
  const { t } = useTranslation();
  const isLogin = useIsAuthenticated();

  useEffect(() => {
    msalInstance.logoutPopup().then(() => {});
  }, []);

  return (
    <LoginPageContainer>
      <PanelContainer container={true} lg={8} item={true} sm={12}>
        <Grid container={true}>
          <CenterPanel item={true} sm={12} xs={12}>
            <Box display="flex" flexDirection="column" alignItems="center" rowGap={2}>
              {!isLogin && (
                <>
                  <CheckOutlined fontSize="large" />
                  <Typography color="black" align="center" variant="h3" fontWeight={600}>
                    {t('have_been_logged_out')}
                  </Typography>
                  <Link to="/login">{t('sign_in')}</Link>
                </>
              )}
              {isLogin && <CircularProgress />}
            </Box>
          </CenterPanel>
        </Grid>
      </PanelContainer>
    </LoginPageContainer>
  );
};

export default Logout;
