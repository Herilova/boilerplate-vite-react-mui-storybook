import { CssBaseline, ThemeProvider } from '@mui/material';
import { withThemeFromJSXProvider } from '@storybook/addon-themes';
import theme from '../src/themes/theme';
import React from 'react';

/* snipped for brevity */

export const decorators = [
  (Story) => (
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <Story />
      </ThemeProvider>
    </React.StrictMode>
  ),
];

/** @type { import('@storybook/react').Preview } */
const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;